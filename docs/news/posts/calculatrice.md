---
authors:
  - frnouv
date: 
    created: 2024-09-02
    updated: 2024-09-09
excerpt: Tout ce que vous avez toujours voulu savoir sur les calulatrices et le mode examen
tags:
    - calculatrices
    - numworks
    - casio
    - ti
---

# Les calculatrices au lycée

## Quelle est la différence entre une calculatrice collège et lycée ?

La calculatrice de collège se distingue d’abord et en règle générale par son design, souvent plus enfantin, plus coloré et avec un écran souvent de petite résolution. Pour une raison simple : c’est que la calculatrice de collège n’embarque pas de mémoire et ne dispose ainsi d’aucune fonction et d’aucune programmation en dehors du calcul simple (addition, division, soustraction, multiplication). Au contraire, la calculatrice de lycée embarque avec elle de nombreuses options, fonctions et programmes pour répondre à des problèmes mathématiques beaucoup plus élevés : graphiques, calculs matriciels, statistiques, inéquations, nombres complexes, etc. En cela, son design est en général plus élaboré, avec un écran plus grand et un boîtier souvent plus grand et plus épais pour pouvoir fournir des performances à la hauteur des exigences du lycée.

La calculatrice de lycée dispose en général d’un port USB grâce auquel on peut la relier à son ordinateur portable d’étudiant, permettant d’installer de nouveaux programmes et des fonctions. Elle affiche bien plus de fonctionnalités et nécessite à cet égard une prise en main plus longue.

## Le mode examen

Le mode Examen est une fonctionnalité implémentée sur certains modèles de calculatrices et qui désactive, de manière provisoire ou définitive, l’accès à la mémoire de la calculatrice.<br>
Depuis la session 2020,  la [circulaire n° 2015-178 du 1er octobre 2015](https://www.education.gouv.fr/pid285/bulletin_officiel.html?cid_bo=94844){target="_blank"} relative à l'utilisation des calculatrices électroniques aux examens et concours de l'enseignement scolaire est mise en œuvre.

Les matériels autorisés sont les suivants :

1. Les **calculatrices non programmables** sans mémoire alphanumérique.

2. les **calculatrices avec mémoire alphanumérique et/ou avec écran graphique qui disposent d’une fonctionnalité « mode examen »** répondant aux spécificités suivantes :
  
    – la **neutralisation temporaire de l’accès à la mémoire** de la calculatrice ou l’effacement définitif de cette mémoire ;

    – le **blocage de toute transmission de données**, que ce soit par wifi, Bluetooth ou par tout autre dispositif de communication à distance

    – la **présence d’un signal lumineux clignotant** sur la tranche haute de la calculatrice, attestant du passage au « mode examen » 

    – la **non-réversibilité du « mode examen » durant toute la durée de l’épreuve**. La sortie du « mode examen » nécessite une connexion physique, par câble, avec un ordinateur ou une calculatrice


## Recommandations

Il est **très important** d'apprendre à utiliser la calculatrice lors des années lycées afin de ne pas être pris au dépourvu lors de l'examen de fin de cursus. Il ne faut surtout pas la découvrir le jour de l'examen ! Les automatismes doivent être bien ancrés.


Les calculatrices généralement utilisées au lycée sont :

=== "Numworks"
    ![Calculatrice numworks](../../assets/images/calc/numworks.png){align=left width=20%}

    Calculatrice Numworks<br><br>Dans un marché de la calculatrice dominé par un constructeur japonais, Casio, et un américain, Texas Instrument, voici le seul **constructeur français : Numworks**.  NumWorks est une société française qui a lancé en France à l’été 2017 une calculatrice graphique visant un public lycéen.<br><br>La calculatrice a été conçue pour être beaucoup plus intuitive : elle s’inspire en effet des codes d’interface des smartphones (avec une interface principale organisée en applications), ainsi que ceux des consoles de jeux vidéo (avec des touches directionnelles s’inspirant de celles des manettes d’une console). La calculatrice NumWorks a été rapidement vendue à plusieurs milliers d’exemplaires auprès des lycéens français, principalement grâce à un design fin, à l’interface Python, aux recommandations de nombreux professeurs et à la création d’une communauté d’utilisateurs proposant en ligne diverses applications.<br><br>Une calculatrice au design étonnant. Très belle, légère, esthétique. D’un usage particulièrement simple, elle est très adapté pour le lycée, en particulier pour un public qui ne souhaite pas particuilèrement passer trop de temps à comprendre comment fonctionne sa calculatrice.

=== "TI 83"
    ![Calculatrice numworks](../../assets/images/calc/ti83.png){align=left width=20%}

    Calculatrice TI 83 Premium CE edition python<br><br>Adaptée à toutes les matières scientifiques du lycée avec cette calculatrice graphique TI-83 Premium CE Edition Python de Texas Instruments. Elle intègre le langage Python, incontournable et très pratique, et est facile à utiliser. Elle propose également le mode examen, obligatoire pour le baccalauréat notamment. Vous pouvez effectuer gratuitement et régulièrement des mises à jour. La calculatrice possède un écran couleurs LCD de bonne résolution, mais parfois un peu fragile.


=== "Casio graph"
    ![Calculatrice numworks](../../assets/images/calc/casio_mathp.png){align=left width=20%}

    Calculatrice Casio Graph Math+<br><br>Un nouveau design, le clavier Classwiz. Avec son grand écran couleur et ses fonctionnalités avancées, la calculatrice Graph Math+ couvre le programme du lycée et répondra à tous les besoins des élèves en parcours scientifiques.
    Elle possède une nouvelle ergonomie favorisant le cheminement scientifique, un menu Python et un Mode Examen simplifiés. La prise en main nécessite un 
    investissement non négligeable. Le prix de cette calculatrice est élevé.


L'équipe pédagogique mathématique recommande la calculatrice Numworks pour tout nouvel achat.
{style="font-size: 24px; color: gray"}
---
sources:

[Ministère de l'éducation nationale et de la jeunesse](https://www.education.gouv.fr/bo/15/Hebdo42/MENS1523092C.htm){target="_blank"}

[L'éclaireur FNAC](https://leclaireur.fnac.com/article/cp27078-mode-examen-ce-qui-change-sur-les-calculatrices-autorisees-en-examen-ou-en-concours/){target="_blank"}
