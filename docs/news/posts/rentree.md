---
authors:
  - frnouv
date: 
    created: 2024-09-02
tags:
  - rentrée
---


# Rentrée 2024-2025

C'est la rentrée !
{ style="font-size:24px;" class="center rounded-sm container justify-center"}

Qui dit rentrée dit … fournitures scolaires !<br>
Voici la liste de rentrée pour toutes mes classes de mathématiques

- [ ] 1 grand cahier **24×32** petits carreaux
- [ ] matériel de géométrie (règle, compas, équerre, rapporteur)
- [ ] calculatrice type « lycée/examen »
