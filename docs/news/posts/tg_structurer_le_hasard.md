---
authors:
  - frnouv
date: 
    created: 2024-09-02
tags:
  - article
---

Un introduction au hasard et aux probabilités, article de Benoît Rittaud paru dans le numéro 220 de Tangente.

![Structurer le hasard 1](images/tg_structurer_le_hasard_1.jpeg)
![Structurer le hasard 2](images/tg_structurer_le_hasard_2.jpeg)
