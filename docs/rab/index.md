---
title: Mathématiciennes et mathématiciens célèbre
collapse: false
---

## Le projet biographie

Ce projet consiste à élaborer une biographie d'une mathématicienne ou d'un mathématicien. Cette biographie devra présenter la personne choisie, à l'instar d'un petit article d'encyclopédie.

## Contenu attendu

Cette biographie comportera idéalement sans pour autant se restreindre :

- Une présentation rapide de la personne choisie ;
- Un parcours de vie mettant l'accent sur les maths et/ou les sciences dans sa vie ;
- Les travaux mathématiques et/ou scientifiques ou faits ayant marqués l'histoire des mathématiques ;
- L'héritage et/ou l'impact actuel de ces travaux (s'il y en a) ;
- …

## Format attendu

Le format est libre, mais la présentation doit être soignée. Vous pouvez aussi bien utiliser un support classique (papier, document imprimé …) qu'un support plus moderne (présentation powerpoint, page web, …) ou surprenant (vidéo, blog audio, bd, manga, …). Soyez créatifs.<br>
**Vous** devez en être l'auteur, **vous** devrez être capable de m'en faire une présentation orale.

## Date de rendu

Votre œuvre devra être rendu au plus tard en fin de semaine 19, soit le vendredi 10 janvier 2025.<br>

## Rappels …

L'extravagance n'est pas un signe de créativité.<br>
Le contenu présenté est important, la forme est un plus.<br>
Le plagiat est inacceptable et sanctionné.<br>
Ce travail n'est pas optionnel.<br>


## Liste pour le projet biographie

??? preuve "Liste non exhaustive de 86 personnalités"

    1. Théano de Crotone (VIe siècle B.C.E.) - Philosophe pythagoricienne, travaux sur la théorie des proportions.
    2. Pythagore (VIe siècle B.C.E.) - Théorème de Pythagore, contributions à la théorie des nombres.
    3. Euclide (IIIe siècle B.C.E.) - Auteur des "Éléments", fondements de la géométrie.
    4. Archimède (287-212 B.C.E.) - Calcul intégral, mécanique, principe d'Archimède.
    5. Liu Hui (220-280) - Commentateur et enrichisseur des "Neuf Chapitres sur l'art mathématique".
    6. Hypatie d'Alexandrie (370-415) - Mathématicienne et astronome, commentaires sur les œuvres d'Apollonius et Diophante.
    7. Zu Chongzhi (429-500) - Calcul de π avec une précision remarquable pour son époque.
    8. Aryabhata (476-550) - Astronome et mathématicien, travaux sur les séries trigonométriques et l'estimation de π.
    9. Brahmagupta (598-668) - Introduction du zéro comme nombre et des nombres négatifs, travaux sur les équations quadratiques.
    10. Al-Khwarizmi (780-850) - Père de l'algèbre, introduction du système décimal.
    11. Fatima al-Fihri (800-880) - Fondatrice de l'université Al Quaraouiyine, contribution à l'éducation mathématique.
    12. Sutayta Al-Mahamali (Xe siècle) - Résolution d'équations complexes.
    13. Ibn al-Haytham (965-1040) - Pionnier de l'optique et des méthodes scientifiques modernes.
    14. Shen Kuo (1031-1095) - Contributions aux mathématiques, à l'astronomie et à la cartographie.
    15. Omar Khayyam (1048-1131) - Résolution géométrique des équations cubiques.
    16. Bhāskara II (1114-1185) - Contributions en algèbre, trigonométrie et calcul différentiel.
    17. Hildegarde de Bingen (1098-1179) - Diffusion des connaissances mathématiques.
    18. Fibonacci (1170-1250) - Introduction des chiffres arabes en Europe, suite de Fibonacci.
    19. Zhu Shijie (1249-1314) - Traités importants sur l'algèbre, méthodes avancées pour résoudre des systèmes d'équations.
    20. Nicole Oresme (1320-1382) - Précurseur du calcul infinitésimal et de la géométrie analytique.
    21. Madhava de Sangamagrama (1350-1425) - Fondateur de l'école du Kerala, pionnier dans le développement des séries infinies.
    22. Jamshīd al-Kāshī (1380-1429) - Calcul de π avec une grande précision.
    23. John Napier (1550-1617) - Invention des logarithmes.
    24. Blaise Pascal (1623-1662) - Géométrie projective, probabilités, création de la Pascaline.
    25. Elena Cornaro Piscopia (1646-1684) - Première femme docteur en philosophie, incluant les mathématiques.
    26. Gottfried Wilhelm Leibniz (1646-1716) - Co-inventeur du calcul infinitésimal, logique mathématique.
    27. Émilie du Châtelet (1706-1749) - Traductrice de Newton, travaux sur l'énergie cinétique.
    28. Leonhard Euler (1707-1783) - Contributions majeures en analyse, théorie des nombres, géométrie.
    29. Maria Gaetana Agnesi (1718-1799) - Première femme professeur de mathématiques, "sorcière d'Agnesi".
    30. Joseph-Louis Lagrange (1736-1813) - Calcul des variations, mécanique analytique.
    31. Pierre-Simon Laplace (1749-1827) - Théorie des probabilités, mécanique céleste.
    32. Mary Somerville (1780-1872) - Mathématicienne et scientifique écossaise, travaux sur l'astronomie et écrits scientifiques.
    33. Sophie Germain (1776-1831) - Théorie des nombres, élasticité.
    34. Carl Friedrich Gauss (1777-1855) - "Prince des mathématiciens", contributions dans de nombreux domaines.
    35. Évariste Galois (1811-1832) - Théorie des groupes, équations algébriques.
    36. Ada Lovelace (1815-1852) - Pionnière de la programmation informatique.
    37. Bernhard Riemann (1826-1866) - Géométrie riemannienne, hypothèse de Riemann.
    38. Georg Cantor (1845-1918) - Théorie des ensembles, concept d'infini en mathématiques.
    39. Sofia Kovalevskaya (1850-1891) - Équations différentielles, mécanique.
    40. Henri Poincaré (1854-1912) - Topologie algébrique, systèmes dynamiques.
    41. David Hilbert (1862-1943) - Formalisation des fondements des mathématiques, espaces de Hilbert.
    42. Grace Chisholm Young (1868-1944) - Théorie des ensembles, géométrie.
    43. Emmy Noether (1882-1935) - Algèbre abstraite moderne, théorème de Noether en physique.
    44. Mary Cartwright (1900-1998) - Théorie du chaos, analyse complexe.
    45. Rózsa Péter (1905-1977) - Pionnière de l'informatique théorique, travaux sur les fonctions récursives.
    46. Grace Hopper (1906-1992) - Pionnière de l'informatique, développeuse du premier compilateur.
    47. John von Neumann (1903-1957) - Informatique, théorie des jeux, mécanique quantique.
    48. Kurt Gödel (1906-1978) - Théorèmes d'incomplétude, logique mathématique.
    49. André Weil (1906-1998) - Géométrie algébrique, théorie des nombres.
    50. Olga Taussky-Todd (1906-1995) - Algèbre matricielle, théorie des nombres.
    51. Dorothy Vaughan (1910-2008) - Pionnière de l'informatique à la NASA.
    52. Alan Turing (1912-1954) - Informatique théorique, cryptanalyse.
    53. Paul Erdős (1913-1996) - Combinatoire, théorie des graphes, théorie des nombres.
    54. Julia Robinson (1919-1985) - Décidabilité et indécidabilité en mathématiques.
    55. Katherine Johnson (1918-2020) - Calculs pour les premiers vols spatiaux américains.
    56. Yvonne Choquet-Bruhat (1923-) - Première femme élue à l'Académie des sciences française, physique mathématique.
    57. Evelyn Boyd Granville (1924-) - Une des premières femmes afro-américaines docteur en mathématiques, travaux pour la NASA.
    58. Benoit Mandelbrot (1924-2010) - Géométrie fractale.
    59. Cathleen Synge Morawetz (1923-2017) - Équations aux dérivées partielles, théorie des ondes.
    60. Dusa McDuff (1945-) - Mathématicienne britannique, travaux en géométrie symplectique.
    61. Margaret Hamilton (1936-) - Informaticienne et mathématicienne, pionnière du développement logiciel pour le programme spatial Apollo.
    62. John Horton Conway (1937-2020) - Théorie des groupes, théorie des nombres, inventeur du "Jeu de la vie".
    63. Stephen Hawking (1942-2018) - Cosmologie théorique, trous noirs.
    64. Karen Uhlenbeck (1942-) - Prix Abel, géométrie différentielle, analyse géométrique.
    65. Cheryl Praeger (1948-) - Mathématicienne australienne, travaux en théorie des groupes et combinatoire.
    66. Fan Chung (1949-) - Mathématicienne américano-taïwanaise, spécialiste en théorie des graphes et combinatoire.
    67. Shirley Ann Jackson (1946-) - Physique théorique, première femme afro-américaine docteur du MIT.
    68. Ingrid Daubechies (1954-) - Théorie des ondelettes, traitement du signal.
    69. Shafi Goldwasser (1958-) - Prix Turing, cryptographie, complexité.
    70. Grigori Perelman (1966-) - Résolution de la conjecture de Poincaré, topologie géométrique.
    71. Elon Lindenstrauss (1970-) - Médaille Fields, théorie ergodique.
    72. Wendy Tan White (1970-) - IA et robotique, promotion des femmes dans les STEM.
    73. Cédric Villani (1973-) - Médaille Fields, analyse fonctionnelle, équations aux dérivées partielles.
    74. Manjul Bhargava (1974-) - Médaille Fields, théorie algébrique des nombres.
    75. Terence Tao (1975-) - Médaille Fields, analyse harmonique, équations aux dérivées partielles.
    76. Eugenia Cheng (1976-) - Théorie des catégories, vulgarisation mathématique.
    77. Maryam Mirzakhani (1977-2017) - Médaille Fields, géométrie et systèmes dynamiques.
    78. Caucher Birkar (1978-) - Médaille Fields, géométrie algébrique.
    79. Artur Avila (1979-) - Médaille Fields, systèmes dynamiques, théorie spectrale.
    80. Akshay Venkatesh (1981-) - Médaille Fields, théorie des nombres, formes automorphes.
    81. Melanie Wood (1981-) - Théorie probabiliste des nombres, combinatoire algébrique.
    82. Po-Shen Loh (1982-) - Éducation mathématique, coach Olympiades.
    83. June Huh (1983-) - Médaille Fields, géométrie algébrique, combinatoire.
    84. Alessio Figalli (1984-) - Médaille Fields, calcul des variations, équations aux dérivées partielles.
    85. Maryna Viazovska (1984-) - Médaille Fields, problème d'empilement de sphères.
    86. Peter Scholze (1987-) - Médaille Fields, géométrie arithmétique.

