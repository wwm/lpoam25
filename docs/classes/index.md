---
title: Présentation
---

Chaque niveau est accessible via un répertoire dédié. Quel que soit le niveau, le répertoire contient une page d'introduction qui décrit le programme dans ses grands lignes et donne une progression annuelle.

Les programmes présentés ici sont ceux de l'année scolaire en cours, 2024/2025 en l'occurence.

Les ressources utilisées sont :

- [Programme de mathématiques de seconde générale et technologique :fontawesome-solid-file-pdf: :fontawesome-solid-arrow-up-right-from-square:](https://eduscol.education.fr/document/24553/download){target="_blank"}
- [Programme de mathématiques de première générale :fontawesome-solid-file-pdf: :fontawesome-solid-arrow-up-right-from-square:](https://eduscol.education.fr/document/24565/download){target="_blank"}
- [Programme de mathématiques de première technologique, séries STD2A, STHR, STI2D, STL, STMG et ST2S :fontawesome-solid-file-pdf: :fontawesome-solid-arrow-up-right-from-square:](https://eduscol.education.fr/document/24553/download){target="_blank"}


Les [attendus de fin de collège :fontawesome-solid-file-pdf: :fontawesome-solid-arrow-up-right-from-square:](https://www.mathsendirect.fr/programmes/cycle_4_3eme_attendus_de_fin_d_annee.pdf){target="_blank"} sont considérés comme acquis. En cas de doute vous pouvez consulter la rubrique [Savoirs acquis](../acquis/index.md)

Source:

[Éduscol — Programmes et ressources en mathématiques - voie GT :fontawesome-solid-arrow-up-right-from-square:](https://eduscol.education.fr/1723/programmes-et-ressources-en-mathematiques-voie-gt){target="_blank"}