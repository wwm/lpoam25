---
title: Seconde générale et technique
---

## Progression

::timeline::
[
    {
        "title": "Présentation",
        "sub_title": "1",
        "content": "Réactivation automatismes troisième",
        "icon": ":fontawesome-solid-person-chalkboard:",
    },
    {
        "title": "Ensembles de nombres et intervalles",
        "sub_title": "2, 3, 4",
        "content": "...",
        "icon": ":material-format-superscript:"
    },
    {
        "title": "Généralités sur les fonctions",
        "sub_title": "5, 6, 7",
        "content": "...",
        "icon": ":material-function:"
    },
]
::/timeline::
