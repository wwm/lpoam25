---
title: Fonctions (Généralités)
---

## Révisions troisième

<iframe src="https://coopmaths.fr/alea/?uuid=0eecd&id=3F10-1&alea=6TPK&uuid=4daef&id=3F10-3&n=8&d=10&s=5&alea=zrem&i=1&cd=1&uuid=ba520&id=3F10-2&n=4&d=10&s=2&s2=1&s3=1&alea=yHSD&i=1&cd=1&uuid=6c6b3&id=3F10-4&n=1&d=10&s=5&s2=false&alea=4Amv&i=1&cd=1&uuid=c9382&id=3F10-5&n=8&d=10&s=3&s2=3&s3=5&alea=ZOR4&i=1&cd=1&uuid=8a78e&id=3F10-6&n=7&d=10&s=3&s2=1&s3=4&alea=Mxrl&i=1&cd=1&uuid=afb2f&id=3F12-3&n=3&d=10&s=5&alea=OJFy&i=1&cd=1&uuid=17c65&id=3F20-2&n=13&d=10&s=3&s2=11&s3=9&s4=3&alea=1S5P&i=1&cd=1&uuid=b8b33&id=3F21-2&n=4&d=10&s=3&alea=dwMI&i=1&cd=1&uuid=056fa&id=3F21-3&n=4&d=10&s=3&s2=3&s3=3&s4=3&alea=ABxh&i=1&cd=1&v=eleve&es=1211000&title=" width="100%" height="850px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Courbe représentative

### Montrer qu'un point appartient ou non à une courbe

<iframe src="https://coopmaths.fr/alea/?uuid=36795&id=2F20-1&n=6&d=10&s=4&s2=3&alea=Btge&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Calculer des coordonnées de points appartenant à une coube connaissant l'abscisse ou l'ordonnée 

<iframe src="https://coopmaths.fr/alea/?uuid=ec059&id=2F20-2&n=5&d=10&s=4&s2=3&alea=i1T5&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Parité de fonctions

<iframe src="https://coopmaths.fr/alea/?uuid=1e362&id=2F25-2&n=4&d=10&cd=1&v=eleve&es=2010000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>
