---
title: Ensembles de nombres, intervalles et valeurs absolues
---
## Rappels

![Ensembles](images/ensembles.png)

![Intervalles](images/intervalles.png)

## Ensembles

<iframe src="https://coopmaths.fr/alea/?uuid=25fb4&id=2N14-1&n=12&d=10&s=10&alea=j8T8&i=1&cd=1&cols=2&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Intervalles

### Union

<iframe src="https://coopmaths.fr/alea/?uuid=bb947&id=can2N04&n=10&d=10&cd=1&v=eleve&es=0211000&i=1&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Intersection

<iframe src="https://coopmaths.fr/alea/?uuid=e356a&id=can2N03&n=10&d=10&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Challenge : union/intersection niveau II

<iframe src="https://coopmaths.fr/alea/?uuid=dc2a5&id=2N11-2&n=10&d=10&cd=1&v=eleve&es=0111000&i=1&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Intervalles et inéquations

<iframe src="https://coopmaths.fr/alea/?uuid=31c01&id=2N11-1&n=8&d=10&cd=1&v=eleve&es=0111000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>


## Valeurs absolues

### Utiliser la notion de valeur absolue

<iframe src="https://coopmaths.fr/alea/?uuid=0d8b3&id=2N15-1&n=15&d=10&s=1&cd=1&cols=3&v=eleve&es=0111000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Résoudre des équations

<iframe src="https://coopmaths.fr/alea/?uuid=e471c&id=2N15-2&n=10&d=10&s=1&cd=1&cols=2&v=eleve&es=0111000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>