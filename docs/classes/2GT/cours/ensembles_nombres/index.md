---
title: Ensembles de nombres, intervalles et valeurs absolues
---

## Les ensembles de nombres

!!! definition "$\mathbb{N}$ : les entiers naturels"
    ℕ = {0, 1, 2, 3, 4, 5, ...} est l'ensemble des entiers naturels.

    !!! example "Exemple"
        7 ∈ ℕ car 7 est un entier naturel.

        -3 ∉ ℕ car -3 n'est pas un entier naturel.

!!! definition "$\mathbb{Z}$ : les entiers relatifs"
    ℤ = {..., -3, -2, -1, 0, 1, 2, 3, ...} est l'ensemble des entiers relatifs.

    !!! example "Exemple"
        -5 ∈ ℤ car -5 est un entier relatif.

        2,5 ∉ ℤ car 2,5 n'est pas un entier relatif.

!!! definition "$\mathbb{Q}$ : les rationnels"
    ℚ est l'ensemble des nombres rationnels. Les nombres rationnels peuvent s'écrire sous la forme d'une fraction dont le numérateur et le dénominateur sont des entiers (le dénominateur étant non nul). Par exemple : 0, 3/11, 18, -5/6, 1,666...

    !!! example "Exemple"
        √2 ∉ ℚ car √2 ne peut pas s'écrire sous forme d'une fraction d'entiers.
        π ∉ ℚ car π ne peut pas s'écrire sous forme d'une fraction d'entiers.

!!! definition "$\mathbb{R}$ : les réels"
    ℝ est l'ensemble des nombres réels. Les nombres réels comprennent tous les nombres rationnels ainsi que les nombres irrationnels.

    !!! example "Exemple"
    Les nombres tels que √2, π, e sont des nombres irrationnels.

!!! method "Propriété"
    ℕ ⊂ ℤ ⊂ ℚ ⊂ ℝ

!!! preuve "Remarques"

    On peut rajouter une étoile (*) en «exposant» pour indiquer que l'on considère l'ensemble privé de $0$.

    On peut rajouter plus (+) ou moins (-) également en exposant pour indoquer que l'on se restreint aux nombres positifs (+) ou négatifs (-) de cet ensemble.

    - ℕ*: ensemble des entiers naturels non nuls
    - ℤ*: ensemble des entiers relatifs non nuls
    - ℚ*: ensemble des nombres rationnels non nuls
    - ℝ*: ensemble des nombres réels non nuls
    - ℝ⁺: ensemble des réels positifs ou nuls [0 ; +∞[
    - ℝ⁺*: ensemble des réels strictement positifs ]0 ; +∞[

## Intervalles

!!! definition "Définition"
    Soient a et b deux nombres réels tels que a < b :

    1. [a ; b] = {x ∈ ℝ | a ≤ x ≤ b} est l'ensemble des nombres réels x tels que a ≤ x ≤ b
    2. ]a ; b[ = {x ∈ ℝ | a < x < b} est l'ensemble des nombres réels x tels que a < x < b
    3. [a ; b[ = {x ∈ ℝ | a ≤ x < b} est l'ensemble des nombres réels x tels que a ≤ x < b
    4. ]a ; b] = {x ∈ ℝ | a < x ≤ b} est l'ensemble des nombres réels x tels que a < x ≤ b

    !!! example "Exemple"
        Par exemple, l'intervalle [-3 ; 2] est composé des nombres réels x tels que -3 ≤ x ≤ 2.

        On a notamment :

        - ]-∞ ; +∞[ = ℝ

        - [0 ; +∞[ = ℝ⁺

        - ]0 ; +∞[ = ℝ⁺*

### Intervalles non bornés

!!! definition "Définition"

    Soit a un nombre réel :

    1. [a ; +∞[ est l'ensemble des nombres réels x tels que x ≥ a
    2. ]a ; +∞[ est l'ensemble des nombres réels x tels que x > a
    3. ]-∞ ; a] est l'ensemble des nombres réels x tels que x ≤ a
    4. ]-∞ ; a[ est l'ensemble des nombres réels x tels que x < a

    !!! example "Exemple"
        ℝ* = ]-∞ ; 0[ ∪ ]0 ; +∞[

### Union et intersection

!!! definition "Définition"

    Soient I et J deux intervalles.

    - L'union I ∪ J est l'ensemble des nombres appartenant à I ou à J.

    - L'intersection I ∩ J est l'ensemble des nombres appartenant à I et à J.

    !!! example "Exemple"

        Soit I = [2 ; 5] et J = [4 ; 7]. Représenter les intervalles I et J ainsi que leur union et leur intersection.

## Valeurs absolues

!!! definition "Définition"

    La valeur absolue d'un nombre réel x est notée |x|. Sa valeur est :

    - x si x est positif ou nul

    - -x si x est négatif

    !!! example "Exemple"

        |3| = 3 et |-3| = 3
        
        |-π| = π et |√2| = √2

!!! note "Propriété"

    ∀x ∈ ℝ, |x| ≥ 0

    !!! example "Exemple"

        |-2| = 2 > 0

        |3| = 3 > 0

        |0| = 0

        La distance entre 2 et 5 est égale à |2 - 5| = |-3| = 3.