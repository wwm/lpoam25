---
title: Fonctions - Géńeralités
---

## Activité

Sur un circuit automobile, un pilote de course procède à des essais sur un circuit fermé. Sur un tour lancé de sa voiture, des enregistreurs de vitesse placés sur le circuit, ainsi qu’un capteur de vitesse embarqué, ont permis d’établir le tableau et le graphique suivants.

![activité 1](images/activite_01.png)

1. Complétez les valeurs manquantes du tableau
2. Combien de fois le pilote a-t-il atteint la vitesse de 160 km/h ?<br>

    ---

    ---

3. Que pensez-vous des deux affirmations suivantes (**argumentez votre réponse**) :
      + « À toute distance $d$ comprise entre 0 et 15 km correspond une vitesse unique de la voiture de course. »

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

      + « À toute vitesse $v$ comprise entre 50 et 220 km/h correspond une distance unique. »

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

    ---

## Définitions et vocabulaire

!!! definition "Fonction"

    Soit $\mathcal{D}$ un intervalle ou une réunion d'intervalles de $\mathbb{R}$ non vide.

    Définir une **fonction** $f$ de $\mathcal{D}$ dans $\mathbb{R}$ (on dit aussi sur $\mathcal{D}$), c'est associer à tout nombre de $\mathcal{D}$ un **unique** réel noté $f(x)$.

    On note :

    $\begin{align}
    f : \mathcal{D} & \rightarrow \mathbb{R} \\
                  x & \mapsto f(x)
    \end{align}$

!!! definition "Vocabulaire"

    - l'ensemble $\mathcal{D}$ est appelé l'**ensemble de définition** de $f$

    - le réel $x$ est appelé la **variable**

    - le réel $f(x)$ est appelé l'**image** de $x$ par $f$

    - le réel $x$ est appelé l'**antécédent** par la fonction $f$ du réel $f(x)$


Il y a différentes manières pour définir une fonction :

- par la donnée d'une formule de calcul, on fit une formule explicite :

    Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=x^2+3x$.<br>
    On la note : <br>

    $\begin{align}
    f : \text{______} & \rightarrow \text{______} \\
    \\
                  x & \mapsto \text{____________}
    \end{align}$

    Complétez :

    $f(4)= \text{____________}$ donc l'image de $4$ par $f$ vaut $\text{____________}$

<br>

- par la donnée d'un tableau de valeurs :

    ![tableau de valeurs](images/tableau_valeurs.png){style="width: 350px"}

    $\mathcal{D}_g = \text{____________}$

    $0$ est $\text{________________________}$ de $2$ <br>
    $3$ est $\text{________________________}$ de $-2$ <br>

- par la donnée d'une courbe :

    ![courbe](images/courbe.png){style="width: 750px;"}

    Pour les deux fonctions $f$ et $g$ donnez :

      - $\mathcal{D}_f = \text{____________}$<br>

      - $\mathcal{D}_g = \text{____________}$<br>

      - Les images de 0, 2 et 4 pour les deux courbes

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

      - Les antécédents de 1, -2 et 4 pour les deux courbes

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

        ---

## Représentation graphique

### Définition

!!! definition "Courbe représentative"

    Soit $f$ une fonction définie sur $\mathcal{D}_f$. On munit le plan d'un repère.

    La **courbe représentative** de la fonction $f$  dans ce repère notée $\mathcal{C}_f$ est l'ensemble des points $M(x;y)$ tels que :
    $\left\{\begin{align}
        x \in \mathcal{D}_f \\
        y = f(x)
    \end{align}\right.$

    On dit que $\mathcal{C}_f$ a pour équation $y=f(x)$.

### Comment construire une courbe représentative manuellement

1. On construit un tableau de valeurs
2. On trace les points dans un repère
3. On relie les points « de manière fluide »


Activité :

Construire dans ce repère la courbe $\mathcal{C}_f$ représentative de la fonction $f$ définie par $f(x)=x^3-3x$.

![repere](images/repere.png)

Indiquez si les points suivants appartiennent ou non à $\mathcal{C}_f$ :

$\begin{align}
 A(1.1; -2.1) \quad & \text{______} \quad \mathcal{C}_f \\
 B(-10; 130) \quad & \text{______} \quad \mathcal{C}_f \\
 C(\sqrt{3}; 0) \quad & \text{______} \quad \mathcal{C}_f \\
 D(10; 970) \quad & \text{______} \quad \mathcal{C}_f \\
 E(10; 130) \quad & \text{______} \quad \mathcal{C}_f \\
 F(-\sqrt{3}; 0) \quad & \text{______} \quad \mathcal{C}_f 
\end{align}$

Rappel: On utilise le symbole mathématique $\boldsymbol{\in}$ pour indiquer qu'un point appartient à une courbe, dans le cas contraire on utilise le symbole $\boldsymbol{\notin}$.

### Utilisation de la calculatrice


---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---


---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

### Utilisation du logiciel geogebra


---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---


---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---

---
