---
title: Polynômes du second degré — Généralités
---

Dans toute cette partie, nous considérerons le trinôme :

$\begin{align}
f : \mathbb{R}  & \rightarrow \mathbb{R} & \\
              x & \mapsto ax^2+bx+c  & \text{avec } a\in \mathbb{R}^*,\, b\in \mathbb{R},\, c \in \mathbb{R}\\
\end{align}$<br>

## Calcul du discriminant d'un trinome du second degré

Comment construire une fonction python qui calcule le déterminant ?

Il faut commencer par lui donner un nom qui sera explicite, comme par exemple : ```p2d_delta```.<br>
La **convention de nommage** est simple :

- on prefixera les noms de toutes nos fonction par ```p2d``` qui rappelera que ce sont des fonctions qui appartiennent au domaine des **{==p==}**olynômes du **{==2==}**<sup>nd</sup> **{==d==}**egré.
- suit un nom qui indique ce que fait la fonction : comme elle calcule le discriminant d'un polynôme du second degré on choisit delta car le symbole couramment utilisé pour le discriminant est $\Delta$

Elle prend 3 paramètres : ```a```, ```b```, et ```c```. Ce sont les coefficients de notre polynôme.

Ensuite commence le **corps** de la fonction. Ce sont toutes les lignes indentées qui suivent la déclaration de la fonction.<br>

Chaque fonction peut se voir attribuer un paragraphe de documentation indiquant son rôle et décrivant les paramètres ; ce paragraphe se place immédiatement après la déclaration de la fonction et est entouré entre deux « triple double quotes », c'est-à-dire : ```"""```.

Tout ce qui suit est le code à proprement parler, le code qui sera exécuté lors de l'appel de la fonction.<br>
Si une fonction renvoie une valeur, il faudra utiliser le mot clé 
    ```#!python return```


Il faut remplacer les ```#!python ...``` par les instructions calculant le discriminant dans le code ci-dessous.<br>
Vous pouvez tester vos propositions avant d'en demander la correction.<br>

Go !

{{ IDE('scripts/p2_01.py') }}

## Calcul du nombre de racines réelles d'un trinôme

La fonction ```#!python def p2d_nombre_de_solutions(a,b,c)``` doit renvoyer un entier qui indique le nombre de racines réelles d'un polynôme du second degré de coefficient $a$, $b$ et $c$.

Dans le code suivant, insérez le paragraphe de documentation ainsi que les instruction nécessaires :

{{ IDE('scripts/p2_02.py') }}

## Construction de la fonction signe

La fonction signe ```#!python sgn``` est définie (mathématiquement) ainsi :

$\begin{align}
\text{sgn}\,:\, \mathbb{R} & \rightarrow \{-1,0,1\} \\
                    x & \mapsto \left\{
                                    \begin{array}{rl}
                                        -1 & \text{Si }x<0 \\
                                         0 & \text{Si }x=0 \\
                                         1 & \text{Si }x>0 \\
                                    \end{array}
                                \right.
\end{align}$

Nous allons créer cette fonction en python en la nommant  ```#!python def sgn(x)```. Nous ne la préfixons pas par ```p2d_``` car cette fonction est une fonction générale qui ne s'applique pas uniquement aux polynômes du second degré.

{{ IDE('scripts/p2_03.py') }}
