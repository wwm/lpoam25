# --- PYODIDE:code --- #

def p2d_delta(a,b,c):
    
    """ Calcule et renvoie le discriminant d'un polynôme du second degré.

        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """
    
    delta = ...
    return delta

# --- PYODIDE:corr --- #

def p2d_delta(a,b,c):
    
    """ Calcule et renvoie le discriminant d'un polynôme du second degré.

        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """
    
    delta = b**2 - 4*a*c
    return delta

# --- PYODIDE:tests --- #

assert p2d_delta(1,-3,2) == 1
assert p2d_delta(1,2,1) == 0
assert p2d_delta(1,3,-2) == 17

# --- PYODIDE:secrets --- #

assert p2d_delta(1,-3,2) == 1
assert p2d_delta(1,2,1) == 0
assert p2d_delta(1,3,-2) == 17

for a in range(-5,6):
    for b in range(-5,6):
        for c in range(-5,6):
            assert p2d_delta(a,b,c) == b*b-4*a*c
