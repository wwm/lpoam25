# --- PYODIDE:env --- #
def p2d_delta(a,b,c):

    """ Calcule et renvoie le discriminant d'un polynôme du second degré.

        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """

    delta = b**2 - 4*a*c
    return delta

# --- PYODIDE:code --- #

def p2d_nombre_de_solutions(a,b,c):
    
    """ 
        ...
    """
    
    delta = p2d_delta(a,b,c)
    
    if delta<0:
        ...
    elif delta==0:
        ...
    else:
        ...
    
    return ...

# --- PYODIDE:corr --- #

def p2d_nombre_de_solutions(a,b,c):
    
    """ 
        Renvoie un entier indiquant le nombre de racines réelles d'un polynomes du second degré de coefficients a, b et c.
        
        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """
    
    delta = p2d_delta(a,b,c)
    
    if delta<0:
        nombre_de_solutions=0
    elif delta==0:
        nombre_de_solutions=1
    else:
        nombre_de_solutions=2
        
    return nombre_de_solutions

# --- PYODIDE:tests --- #

assert p2d_nombre_de_solutions(1,-3,2) == 2
assert p2d_nombre_de_solutions(1,2,1) == 1
assert p2d_nombre_de_solutions(1,3,-2) == 2

# --- PYODIDE:secrets --- #

for a in range(-5,6):
    for b in range(-5,6):
        for c in range(-5,6):
            if b*b-4*a*c<0:
                assert p2d_nombre_de_solutions(a,b,c) == 0
            elif b*b-4*a*c==0:
                assert p2d_nombre_de_solutions(a,b,c) == 1
            else:
                assert p2d_nombre_de_solutions(a,b,c) == 2
