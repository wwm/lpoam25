# --- PYODIDE:env --- #
def p2d_delta(a,b,c):

    """ Calcule et renvoie le discriminant d'un polynôme du second degré.

        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """

    delta = b**2 - 4*a*c
    return delta

def p2d_nombre_de_solutions(a,b,c):
    
    """ 
        Renvoie un entier indiquant le nombre de racines réelles d'un polynomes du second degré de coefficients a, b et c.
        
        Paramètres:
            a,b,c --- les coefficients du polynôme f(x)=ax²+bx+c
    """
    
    delta = p2d_delta(a,b,c)
    
    if delta<0:
        nombre_de_solutions=0
    elif delta==0:
        nombre_de_solutions=1
    else:
        nombre_de_solutions=2
        
    return nombre_de_solutions


# --- PYODIDE:code --- #

def sgn(x):
    
    """ 
        ...
    """

    ...
    

# --- PYODIDE:corr --- #

def sgn(x):
    
    """ Renvoie le signe du réel x, soit :
        • -1 si x est strictement négatif
        •  0 si x est nul
        •  1 si x est strictement positif
        
        Paramètre:
            x --- un réel
    """

    if x<0:
        return -1
    elif x==0:
        return 0
    else:
        return 1


# --- PYODIDE:tests --- #

assert sgn(-42) == -1
assert sgn(0)   == 0
assert sgn(42)  == 1

# --- PYODIDE:secrets --- #

for x in range(1, 10):
    assert sgn(-x) == -1
    assert sgn(x)  == 1
assert sgn(0)   == 0
