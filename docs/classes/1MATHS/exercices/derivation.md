---
title: Dérivées
---

## Nombre dérivé

!!! parcoeur "Nombre dérivé"

    Le nombre dérivé en $x_0$ d'une fonction $f$ est la limite, si elle existe, de :

    $$ \lim\limits_{h\to 0} \dfrac{f(x_0+h)-f(x_0)}{h} $$

    Une autre façon d'exprimer cette limite est, en posant $x=x_0+h$ :

    $$ \lim\limits_{h\to 0} \dfrac{f(x)-f(x_0)}{x-x_0} $$

    En général, la première formule est la plus utile pour calculer les nombres dérivés «&nbsp;à la dure&nbsp;».

### Détermination du nombre dérivé par la formule

Dans l'exercice suivant, je vous propose dans un premier temps de calculer le nombre dérivé demandé en utilisant la formule «&nbsp;à la dure&nbsp;», même si la correction propose le calcul de la fonction dérivée puis l'évaluation du nombre dérivé à la valeur demandée.

!!! methode "Exemple du calcul d'un nombre dérivé"

    Soit f la fonction définie sur $\mathbb{R}$ par $f(x)=-2x^2+1$. Calculons le nombre dérivé en $x_0=-1$ de $f$ :

    Nous devons donc déterminer si la limite $\lim\limits_{h\to 0} \dfrac{f(x_0+h)-f(x_0)}{h}$ existe.

    + Calculons dans un premier temps $f(x_0+h)=f(h-1)$ :

        $\begin{align}
            f(h-1) &= -2(h-1)^2+1 \\
                   &= -2(h^2-2h+1)+1 \\
                   &= -2h^2 +4h -2 +1 \\
                   &= -2h^2 +4h -1
        \end{align}$
    
    + Calculons ensuite $f(x_0)=f(-1)$ :

        $\begin{align}
            f(-1) &= -2(-1)^2+1 \\
                  &= -2 + 1 \\
                  &= -1
        \end{align}$

    En remplaçant les expressions calculées par leur valeur nous obtenons :

    $\begin{align}
    \lim\limits_{h\to 0} \dfrac{f(x_0+h)-f(x_0)}{h} &= \lim\limits_{h\to 0} \dfrac{f(h-1)-f(-1)}{h} \\
                                                    &= \lim\limits_{h\to 0} \dfrac{-2h^2 +4h -1 - (-1)}{h} \\
                                                    &= \lim\limits_{h\to 0} \dfrac{-2h^2 +4h -1 + 1}{h} \\
                                                    &= \lim\limits_{h\to 0} \dfrac{-2h^2 +4h}{h} \\
                                                    &= \lim\limits_{h\to 0} \dfrac{-2h(h -2)}{h} \\
                                                    &= \lim\limits_{h\to 0} -2(h -2) & \\
                                                    \\ 
                                                    & \qquad \text{la limite existe car si on remplace } & \\
                                                    & \qquad h\text{ par 0 il n'y a pas de problèmes de division par 0}& \\
                                                    \\
                                                    &= -2(0-2) \\
                                                    &= 4
    \end{align}$

    Donc $f'(-1)=4$.

    ---

    En utilisant la méthode pour calculer la fonctions dérivée :

    $\begin{align}
    f(x) &= -2x^2+1 \\
    \\
    f'(x) &= (-2x^2+1)' \\
          &= (-2x^2)' + (1)' & \text{car }(u+v)'=u'+v' \\
          &= (-2x^2)' + 0 & \text{car }(a)' = 0 \text{ pour toute constante } a \\
          &= -2(x^2)' & \text{car }(\lambda u)'=\lambda(u)' \text{ pour toute constante } \lambda \\
          &= -2(2x^1) & \text{car }(x^n)'=nx^{n-1}\text{ avec ici }n=2 \\
          &= -4x
    \end{align}$

    donc $f'(-1)=-4(-1)=4$.

### Fonctions de références étudiées en seconde

<iframe src="https://coopmaths.fr/alea/?uuid=29202&id=1AN10-1&n=4&d=10&s=5&alea=Vm9g&cd=1&v=eleve&es=0210000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Fonctions aléatoires

<iframe src="https://coopmaths.fr/alea/?uuid=a1ba2&id=can1F14&n=4&d=10&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Détermination par lecture graphique

Le nombre dérivé en $x_0$ d'une fonction $f$ définie sur $\mathbb{R}$ est le coefficient directeur de la tangente en $x_0$ de la courbe représentative $\mathcal{C}_f$ de $f$. Lire graphiquement le coefficient directeur d'une droite revient à déterminer un vecteur directeur $\vec{u}\left(\begin{matrix}u_x \\ u_y\end{matrix}\right)$ de cette droite puis à calculer le coefficient directeur grâce à la formule $\operatorname{coef\_dir}=\dfrac{u_y}{u_x}$.<br>

!!! methode "Lecture graphique"

    Soit une fonction $f$ définie sur $\mathbb{R}$ et représentée graphiquement sur la figure ci-dessous par la courbe en bleu. Soit la tangente à $\mathcal{C}_f$ en 0, la droite en rouge sur le graphique.

    ![Lecture graphique nombre dérivé](images/derivation_lecture_graphique.png){style="float:right; padding: 1em;"}

    Prenons deux points sur la tangente dont les coordonnées sont facilement lisibles, comme par exemple, $A(-4,4)$ et $B(0,1)$.<br>
    Un vecteur directeur de la tangente est $\overrightarrow{AB}\left(\begin{matrix}4 \\ -3\end{matrix}\right)$<br>
    Le coefficient directeur de la tangente vaut donc : $\dfrac{-3}{4}$.<br>
    Donc le nombre dérivé en $0$ de la fonction $f$ est : $f'(0)=-\dfrac{3}{4}$.

<iframe src="https://coopmaths.fr/alea/?uuid=0e984&id=can1F15&n=2&d=10&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="750" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Équation de la tangente

### Avec la formule du cours

Le cours donne directement une formule qui donne une équation de la tangente en $x_0$ d'une fonction $f$ :

$$ y = f(x_0)+(x-x_0)f'(x_0) $$

On peut la réécrire sous la forme :

$\begin{align}
    y &= f(x_0)+(x-x_0)f'(x_0) \\
      &= f'(x_0)x + f(x_0) - x_0f'(x_0) \\
      &= mx+p
\end{align}$

Avec le coefficient directeur $m=f'(x_0)$ et l'ordonnée à l'origine $p=f(x_0) - x_0f'(x_0)$

!!! methode "Détermination d'une équation de la tangente avec la formule"

    Soit $f$ une fonction telle que $f(2)=4$ et $f'(2)=-1$.<br>
    Déterminons l'équation de la tangente $T$ en $2$ à $\mathcal{C}_f$.

    La formule du cours nous donne directement :

    $\begin{align}
    T : y &= f(x_0)+(x-x_0)f'(x_0) \\
       y &= 4 + (x-2)(-1)\\
       y &= 4 - x + 2 \\
       y &= -x + 6
    \end{align}$

<iframe src="https://coopmaths.fr/alea/?uuid=4c8c7&id=1AN11-3&n=2&d=10&s=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="400px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Sans la formule du cours

!!! methode "Détermination d'une équation de la tangente sans la formule"

    Soit $f$ une fonction dérivable sur $\mathbb{R}$ telle que $f(2)=4$ et $f'(2)=-1$.<br>
    Déterminons l'équation de la tangente $T$ en $2$ à $\mathcal{C}_f$.

    On sait que la tangente n'est pas une droite verticale, puisque la fonction est dérivable sur son domaine de définition.
    On en déduit que la tangente admet au point d'abscisse $2$ une équation réduite de la forme : 
    $T : y=mx+p$

    + Détermination de $m$

        On sait que le nombre dérivé en 2 est par définition, le coefficient directeur de la tangente au point d'abscisse 2.<br>
        Par conséquent, on a déjà : $m=f'(2)=-1$<br>
        On en déduit que $T: y=-x+p$<br>

    + Détermination de $p$

        On sait que le point de coordonnées $(2,f(2)=4)$ appartient non seulement à $\mathcal{C}_f$ mais également à $T$. Donc les coordonnées de ce point vérifient l'équation de $T$ :

        $f(2) = 4 = -2 + p$, d'où $p=6$.

    On peut donc conclure que $T: y=-x+6$.

<iframe src="https://coopmaths.fr/alea/?uuid=4c8c7&id=1AN11-3&n=2&d=10&s=2&cd=1&v=eleve&es=0211000&title=" width="100%" height="400px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Dérivées usuelles et formules de dérivation

Rappel des dérivées usuelles à connaître par cœur :

|          $f(x)$          |        $f'(x)$         |
| :----------------------: | :--------------------: |
|           $a$            |          $0$           |
|          $ax+b$          |          $a$           |
|       $ax^2+bx+c$        |        $2ax+b$         |
|      $\dfrac{1}{x}$      |   $-\dfrac{1}{x^2}$    |
|        $\sqrt{x}$        | $\dfrac{1}{2\sqrt{x}}$ |
| $x^n$ ($n\in\mathbb{Z}$) |       $nx^{n-1}$       |

Rappel des formules de dérivations (partie 1) :

$\begin{align}
(u+v)' &= u'+v' & \\
(uv)' &= u'v+uv' & \\
(\lambda u)' &= \lambda u' & \lambda \text{ une constante réelle}\\
(\dfrac{1}{v})' &= -\dfrac{v'}{v^2} \\
(\dfrac{u}{v})' &= \dfrac{u'v-uv'}{v^2} \\
\end{align}$

### Dérivées de polynomes

<iframe src="https://coopmaths.fr/alea/?uuid=ec088&id=1AN14-3&n=4&d=10&s=6&s2=true&s3=false&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>


### Dérivées du type (u+v)'

<iframe src="https://coopmaths.fr/alea/?uuid=a83c0&id=1AN14-4&n=6&d=10&s=5&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Dérivées de type (uv)'

<iframe src="https://coopmaths.fr/alea/?uuid=1a60f&id=1AN14-5&n=6&d=10&s=6&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### dérivées du type (u/v)'

<iframe src="https://coopmaths.fr/alea/?uuid=b32f2&id=1AN14-6&n=6&d=10&s=5&s2=false&cd=1&v=eleve&es=0211000&title=" width="100%"  height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></
