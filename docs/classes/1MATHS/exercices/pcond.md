---
title: Probabilités conditionnelles
---

## Mise en bouche

<iframe src="https://coopmaths.fr/alea/?uuid=ae872&id=2S30-7&n=1&d=10&s=7&alea=Vm0O&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Probabilités conditionnelles et indépendance

<iframe src="https://coopmaths.fr/alea/?uuid=9ccfd&id=1P10&n=2&d=10&s=false&s2=3&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>