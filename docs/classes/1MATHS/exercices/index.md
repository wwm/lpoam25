---
title: Exercices polynômes du second degré
---

## Calcul de discriminant

<iframe src="https://coopmaths.fr/alea/?uuid=731f0&id=1AL20-10&n=6&d=10&s=2&cd=1&cols=2&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Factorisation de trinôme

<iframe src="https://coopmaths.fr/alea/?uuid=334ca&id=1AL21-40&n=6&d=10&s=1&cd=1&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Équations et inéquations

### Équations du second degré

<iframe src="https://coopmaths.fr/alea/?uuid=1be55&id=1AL23-22&n=4&d=10&s=4&s2=4&s3=5&alea=AeVh&cd=1&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Nombre de solutions d'une équation du second degré à paramètre

<iframe src="https://coopmaths.fr/alea/?uuid=fe4df&id=1AL23-23&alea=2ooa&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Inéquation du second degré

<iframe src="https://coopmaths.fr/alea/?uuid=77bcc&id=1AL23-40&alea=vGEn&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Tout en un 

<iframe src="https://coopmaths.fr/alea/?uuid=b9252&id=1AL23-41&n=8&d=10&cd=1&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Étude de la fonction trinome

### Représentation graphique d'un polynôme du second degré

<iframe src="https://coopmaths.fr/alea/?uuid=a896e&id=1AL23-50&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Sens de variations

<iframe src="https://coopmaths.fr/alea/?uuid=16f97&id=1AL22-6&n=4&d=10&s=8&cd=1&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Signes

<iframe src="https://coopmaths.fr/alea/?uuid=c0f97&id=can1F27&n=4&d=10&cd=1&v=eleve&es=0111010&title=" width="100%" height="600" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>
