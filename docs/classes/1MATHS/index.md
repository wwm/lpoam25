---
title: Première Spécialité Math
---

## Progression

::timeline::
[
    {
        "title": "Présentation",
        "sub_title": "1",
        "content": "Réactivation automatismes seconde",
        "icon": ":fontawesome-solid-person-chalkboard:",
    },
    {
        "title": "Polynôme du second degré",
        "sub_title": "2, 3, 4",
        "content": "...",
        "icon": ":material-format-superscript:"
    },
    {
        "title": "Dérivation",
        "sub_title": "5, 6, 7",
        "content": "...",
        "icon": ":material-function:"
    },
    {
        "title": "Probabilités conditionnelles",
        "sub_title": "10, 11, 12",
        "content": "...",
        "icon": ":material-cards-playing-outline:"
    },
    {
        "title": "Application de la dérivation",
        "sub_title": "13, 14",
        "content": "...",
        "icon": ":material-function:"
    },
    {
        "title": "Produit scalaire",
        "sub_title": "15, 16, 19",
        "content": "...",
        "icon": ":material-circle-medium:"
    },
    {
        "title": "Fonction exponentielle",
        "sub_title": "20, 21, 22",
        "content": "...",
        "icon": ":material-exponent:"
    },
    {
        "title": "Généralités sur les suites",
        "sub_title": "23, 24, 25",
        "content": "...",
        "icon": ":material-alpha-n:"
    },
    {
        "title": "Variables aléatoires",
        "sub_title": "28, 29",
        "content": "...",
        "icon": ":material-tray-remove:"
    },
    {
        "title": "Suites de références",
        "sub_title": "30, 31",
        "content": "...",
        "icon": ":material-alpha-n:"
    },
    {
        "title": "Fonctions trigonométriques",
        "sub_title": "31, 34, 35",
        "content": "...",
        "icon": ":material-math-sin:"
    },
    {
        "title": "Applications du produit scalaire",
        "sub_title": "36, 37, 38",
        "content": "...",
        "icon": ":material-circle-medium:"
    },
]
::/timeline::