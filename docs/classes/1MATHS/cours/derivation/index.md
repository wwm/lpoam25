---
title: Dérivées
---

Dans ce chapitre, nous allons définir un outil permettant de déterminer le sens de variation d'une fonctions et de construire une tangente en un point de la courbe représentative de la fonction. La **dérivée** en un point permet de déterminer une approximation de cette fonction en ce point.

## Petit mot sur la notion mathématique de **{++limite++}**

Intuitivement la notion mathématique de limite décris plusieurs faits lorsqu'on fait varier un nombre :

+ par exemple, plus un réel $x$ devient grand, plus son carré $x^2$ devient grand lui aussi. Et, à la *«&nbsp; limite &nbsp;»*, lorsque $x$ *«&nbsp;devient infiniment grand &nbsp;»* alors son carré devient lui aussi *«&nbsp;infiniment grand&nbsp;»*. Mathématiquement on écrit dans ce cas :

    $$
    \lim\limits_{x\to +\infty} x^2 = +\infty
    $$

    Cette notation se lit «&nbsp; la limite de x au carré lorsque x tend vers plus l'infini **diverge** vers plus l'infini &nbsp;».<br>
    On peut définir d'une manière similaire, une limite en $-\infty$ ou des limites qui sont égales à $-\infty$.
    On utilise, comme ci-dessus, le verbe diverger pour indiquer que la limite devient de plus en plus grande positivement ou négativement.

+ on peut aussi avoir un autre cas, celui où à la limite on se rapproche de plus en plus près d'un nombre réel. L'exemple classique est celui de la fonction $f(x)=1+\dfrac{1}{x}$. Plus $x$ *devient grand* et plus la quantité $\dfrac{1}{x}$ *devient petite* et, à la limite, *négligeable*. On traduit mathématiquement ce fait par :

    $$
    \lim\limits_{x\to + \infty} 1+\dfrac{1}{x} = 1
    $$

    Cette notation se lit «&nbsp;la limite de un plus un sur $x$ lorsque $x$ tend vers plus l'infini **vaut** un&nbsp;»

+ Il y a également le cas où on fait tendre $x$ non vers un infini mais vers une valeur réelle. Prenons l'exemple de la fonction $f(x)=\dfrac{1}{(x-2)^2}$. Plus on prend $x$ proche de $2$, plus $x-2$ devient proche de $0$ et plus $\dfrac{1}{(x-2)^2}$ devient infiniment grand en restant positif. Mathématiquement on écrit :

    $$
    \lim\limits_{x \to 2} \dfrac{1}{(x-2)^2} = +\infty
    $$

    Un cas simple est celui où on cherche la limite en un point où une fonction est définie, dans ce cas la limite a pour valeur la valeur de la fonction. Si on prends la fonction $f(x)=x^2-4x+1$ :

    $$
    \lim\limits_{x \to 4} f(x) = f(4) = 1
    $$

!!! warning "Une limite peut ne pas exister ou être indeterminée !"

    Une limite peut ne pas exister comme par exemple $\displaystyle{\lim\limits_{x \to +\infty} \sin{x}}$, on dit alors qu'elle **diverge** ou **qu'elle n'existe pas**.

    Une limite possède ce qu'on appelle des **formes indéterminées**, c'est-à-dire des cas où on ne pas conclure immédiatement. Ce sont les cas où on trouve des résultats comme : $\infty - \infty$, $0 \times \infty$, $\frac{0}{0}$, $\frac{\infty}{\infty}$, $0^0$, $\infty^0$ et $1^\infty$.

    Nous en reparlerons un peu plus en détail l'an prochain.

## Nombre dérivé

### Activité géogébra

<iframe allowfullscreen scrolling="no" title="Limite graphique" src="https://www.geogebra.org/material/iframe/id/fr4dvt8z/width/1920/height/895/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/true/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false" width="1920px" height="650px" class="shadow-splash-min"> </iframe>

Grâce à cette  activité vous pourrez explorer la notion de limite et de tangente.<br>
On peut changer l'équation de la courbe $\mathcal{C}_f$ en donnant $f(x)$ dans la boîte de saisie située en dessous du graphique.<br>

Sur la courbe $\mathcal{C}_f$, on prend un point mobile $M_0(x_0,y_0)$ ainsi qu'un autre point $M(x,y)$ tel que $x=x_0+h$. La valeur de $h$ est modifiable, $h\in [0,000000 1; 0,2]$. Plus $h$ devient petit, plus le droite $(MM_0)$ semble se rapprocher de la tangente à la courbe $\mathcal{C}_f$ en $M_0$. Cela est vérifiable en calculant le coefficient directeur de la droite $C_{(MM_0)}=\dfrac{f(x_0+h)-f(x_0)}{h}$ et en le comparant avec le coefficient directeur de la tangente $T_{M_0}$ en $M_0$ calculé de manière indépendante.<br>
On pourra aussi remarquer que le signe de $C_{T_{M_0}}$, le coefficient directeur de la tangente $T_{M_0}$ à $\mathcal{C}_f$ en $M_0$, semble indiquer le sens de variation de la fonction $f$.

## Nombre dérivé

Soient $f : \mathcal{D}_f \rightarrow \mathbb{R}$ et $x_0 \in \mathcal{D}_f$.

Si la limite $\lim\limits_{h\to 0} \dfrac{f(x_0+h)-f(x_0)}{h}$ existe et vaut $l$, alors on dit que $l$ est le **nombre dérivé** de la fonction $f$ en $x_0$.<br>
On dit aussi que $\bf{f}$ **est dérivable en** $\bf{x_0}$.<br>
La droite $T_{M_0}$ passant par $M_0$ et de coefficient directeur $l$ est appelée **tangente en $\bf{M_0}$ à la courbe $\bf{\mathcal{C}_f}$**.

Remarque :

En posant $x=x_0+h$ on peut réécrire la limite ainsi :

$$
\lim\limits_{h\to 0} \dfrac{f(x_0+h)-f(x_0)}{h} = \lim\limits_{x\to x_0} \dfrac{f(x)-f(x_0)}{x-x_0}
$$

## Fonction dérivée

La fonction qui à tout $x_0$ de $\mathcal{D}$ associe le nombre dérivé de $f$ en $x_0$ est appelée **fonction dérivée de $f$ sur $\mathcal{D}_f$** ou plus simplement **dérivée de $f$ sur $\mathcal{D}_f$**.<br>
On la note $f'$, on dit simplement «&nbsp;f prime&nbsp;».

## Équation de la tangente à une courbe

+ Soit $f$ une fonction dérivable en $x_0$. Une équation de la tangente $T_{M_0}$ en $M_0(x_0, f(x_0))$ est&nbsp;

$$
    y = f(x_0)+(x-x_0)f'(x_0)
$$

## Formules sur les dérivées

Dérivées usuelles

|          $f(x)$          |        $f'(x)$         |
| :----------------------: | :--------------------: |
|           $a$            |          $0$           |
|          $ax+b$          |          $a$           |
|       $ax^2+bx+c$        |        $2ax+b$         |
|      $\dfrac{1}{x}$      |   $-\dfrac{1}{x^2}$    |
|        $\sqrt{x}$        | $\dfrac{1}{2\sqrt{x}}$ |
| $x^n$ ($n\in\mathbb{Z}$) |       $nx^{n-1}$       |

Lorsque les nombres dérivés ou les fonctions dérivées existent, nous avons les résultats suivants&nbsp;:

$$
\begin{align}
(u+v)' &= u'+v' & \\
(uv)' &= u'v+uv' & \\
(\lambda u)' &= \lambda u' & \lambda \text{ une constante réelle}\\
\left(\dfrac{1}{v}\right)' &= -\dfrac{v'}{v^2} \\
\left(\dfrac{u}{v}\right)' &= \dfrac{u'v-uv'}{v^2} \\
(u^n)' &= n u' u^{n-1} & n \in \mathbb{Z} \\
\\
(u(ax+b))' &= au'(ax+b) \\
(\sqrt{ax+b})' &= \dfrac{a}{2\sqrt{ax+b}} \\
\\
[u(\,v(x)\,)]' &= v'(x)u'(v(x)) \\
(u \circ v)' &= v'(u'\circ v) \\
\end{align}
$$