---
title: Théories des ensembles
---

Les ensembles sont d'une importance fondamentale en mathématiques ; en fait, de manière formelle, la mécanique interne des mathématiques (nombres, relations, fonctions, etc.) peut se définir en termes d'ensembles. Il y a plusieurs façons de développer la théorie des ensembles et plusieurs théories des ensembles existent. Il est usuel, au niveau première math spé, de présenter la **Théorie naïve des ensembles**.
<br>
Il est utile d'étudier à un stade précoce des mathématiques une théorie naïve des ensembles, pour apprendre à les manipuler, car ils interviennent à peu près dans tous les domaines des mathématiques. De plus, une bonne compréhension de la théorie naïve est importante comme première approche de la théorie axiomatique.

## Ensembles, éléments et appartenance

Un ensemble désigne intuitivement un rassemblement d’objets <ins>distincts</ins> (les éléments de l'ensemble), «&nbsp;une multitude qui peut être comprise comme une totalité&nbsp;» pour paraphraser Georg Cantor qui est à l'origine de la théorie des ensembles.

On représente graphiquement un ensemble par une «&nbsp;patate qui contient les éléments&nbsp;». On appelle cette représentation un diagramme d'Euler ou un diagramme de Venn. L'image suivante représente un ensemble composé de polygones.

<svg width="450" height="400" version="1.1" xmlns="http://www.w3.org/2000/svg">
	<g stroke="#000" stroke-width="3.5">
		<ellipse cx="225" cy="200" rx="205" ry="175" fill="#e6f0fa"/>
		<g stroke-linejoin="round">
			<rect transform="rotate(24.644)" x="272.9" y="-30.394" width="46.488" height="46.488" fill="#9ade00"/>
			<path d="m281.09 233.89-54.361-23.887 47.867-35.134z" fill="#3cb371"/>
			<path d="m235.2 327.38-25.016-29.542 20.365-32.92 37.602 9.1958 2.8741 38.603z" fill="#fd7118"/>
			<path d="m116.6 256.91 26.929-12.304 24.311 19.257 3.3863 36.318-20.088 26.03-28.436-3.8587-15.371-30.842z" fill="#ff2a2a"/>
			<path d="m105.76 230.16 36.583-10.533 22.896-36.272-88.191 16.11z" fill="#dda0dd"/>
			<path d="m72.92 146.19 11.424-45.042 5.855 32.108 46.593 13.339z" fill="#fd7"/>
			<rect transform="rotate(48.16)" x="155.93" y="-70.574" width="69.101" height="23.166" fill="#364e59"/>
			<path d="m339.24 138.66 42.548-5.6497-13.92 58.209z" fill="#b50000"/>
			<path d="m397 262-35.122 36.771-36.403 0.38891-1.2812-36.382 35.122-36.771 36.403-0.38891z" fill="#4682b4"/>
		</g>
	</g>
</svg>

Les différences entre les diagrammes d'Euleur et de Venn sont minimes et non importantes ici. Nous parlerons donc simplement de diagrammes.

### Ensemble donné en extension

Donner un ensemble en extension consiste simplement à donner le nom de l'ensemble et de lister tous les éléments contenus<br>
On peut par exemple définir un ensemble en disant «&nbsp;A est l'ensemble qui contient les nombres naturels 1, 2 et 5&nbsp;». En notation mathématique on écrit :

$$A=\{1,2,5\}$$

On peut définir de manière similaire $B=\{1,6\}$ et $C=\{4,7\}$.

On peut représenter ces ensembles par le diagramme :

<svg viewBox="0 0 65 44" xmlns="http://www.w3.org/2000/svg">
    <path d="m3 22a14 14 0 1 0 24 0" fill="#edee38"/>
    <path d="m3 22a14 14 0 1 1 24 0" fill="#ee8d82"/>
    <circle cx="50" cy="21" r="14" fill="#7ae97d"/>
    <path d="m3 22a14 14 0 0 0 24 0 14 14 0 0 0-24 0" fill="#c3a4a0"/>
    <text font-size="5">
        <tspan x="1" y="42">B</tspan>
        <tspan x="14" y="36">6</tspan>
        <tspan x="13" y="23">1</tspan>
        <tspan x="1" y="4">A</tspan>
        <tspan x="6" y="14">2</tspan>
        <tspan x="19" y="10">5</tspan>
        <tspan x="44" y="18">4</tspan>
        <tspan x="53" y="28">7</tspan>
        <tspan x="49" y="40">C</tspan>
    </text></svg>

Ce diagramme met en évidence que les ensembles $A$ et $B$ partagent un élément : $1$, mais que l'ensemble $C$ ne partage aucun de ses éléments avec $A$ ou $B$.

### Ensemble donné en compréhension

Définir un ensemble en compréhension consiste «&nbsp;à donner une formule qui permet d'en calculer les éléments&nbsp;». Par exemple pour définir l'ensemble $\mathbb{P}$ des entiers naturels pairs on peut écrire : $\mathbb{P}=\{n\in \mathbb{N} | \frac{n}{2}\in\mathbb{N}\}$ ou encore $\mathbb{P}=\{n\in \mathbb{N} | \exists i\in\mathbb{N}, n=2i\}$ ou encore $\mathbb{P}=\{n\in \mathbb{N} | n[2] \equiv 0 \}$.<br/>
On donne une propriété nécessaire et suffisante pour caractériser les éléments de l'ensemble. Les propriétés utilisées sont respectivement :

* un naturel est pair si et seulement si le résultat de sa division par deux est un naturel ;
* un naturel est pair si et seulement si il existe un autre naturel $i$ dont il est le double ;
* un naturel est pair si et seulement si les reste de sa division euclidienne par deux vaut zéro.

Dans les cas les plus simples on peut utiliser les ellipses, c'est-à-dire les trois petits points, si la compréhension est simple : $\mathbb{P}=\{0,2,4,6,…\}$. Cela n'est en général pas recommandé, mais peut être utilisé au brouillon ou en notes.

### Appartenance

On utilise le symbole $\in$ pour indiquer l'appartenance d'un élément à un ensemble. Par exemple on a $1 \in A$.<br/>
Comme d'habitude, barrer le symbole indique le contraire c'est-à-dire qu'un élément n'appartient pas à un ensemble. Par exemple on a $1 \notin C$.

## Égalité de deux ensembles

Deux ensembles $A$ et $B$ sont égaux **si et seulement si** tous les éléments appartenant à $A$ appartiennent aussi à $B$ **et** si tous les éléments de $B$ appartiennent aussi à $A$, c'est-à-dire qu'ils ont exactement les mêmes éléments. La définition est :

$$A = B \iff x\in A \implies x \in B \land x \in B \implies x \in A$$

Le symbole $\land$ signifie **et**.

## Cardinal d'un ensemble

On appelle **cardinal** d'un ensemble fini le nombre d'éléments qu'il contient, c'est en quelque sorte «&nbsp;sa taille&nbsp;». Pour un ensemble fini $A$, on note son cardinal de différentes manières toutes équivalentes : $\operatorname{Card}(A)=\#A=|A|$.

#### Propriété 

* $|\varnothing|=0$

## Ensembles particuliers

### Ensemble vide

On admet l'existence d'un ensemble particulier : l'ensemble vide. Il ne contient aucun élément, il est noté $\varnothing$ et peut aussi être noté $\{\}$.

### Ensemble univers

On appelle ensemble univers, l'ensemble qui contient tous les éléments utiles dans un contexte, il est noté $\Omega$ (lettre grecque oméga capitale).<br/>Il n'est pas le même dans chaque contexte. Par exemple, si l'on considère les lettres latines, l'ensemble univers sera : $\Omega = \{a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z\}$ ; si l'on considère les valeurs des faces d'un dé D6 on aura : $\Omega = \{1,2,3,4,5,6\}$ ; si l'on considère les faces d'une pièce on aura : $\Omega = \{\operatorname{pile},\operatorname{face}\}$.

### Sous-ensembles

Un ensemble $A'$ est un sous-ensemble de l'ensemble $A$ **si et seulement si** tous les éléments de $A'$ sont également des éléments de $A$. On note cette propriété avec les symbole $\subset$. La définition est :

$$ A' \subset A \iff ( \forall x \in A' \implies x \in A )$$

#### Propriétés
Quels que soient les ensembles $A, B$ et $C$, on a toujours :

* $\varnothing \subset A$
* $A \subset A$
* $A \subset B \subset C \implies A \subset C$
* $A \subset B \land B \subset A \implies A=B$

## Opérations sur les ensembles

### Union

L'union de deux ensembles $A$ et $B$ est l'ensemble des éléments qui appartiennent à $A$ **ou** à $B$. On note cet ensemble $A \cup B$. La définition est :

$$ A \cup B = \{ x | x\in A \lor x\in B\} $$

Le symbole $\lor$ signifie «&nbsp;ou&nbsp;», cette expression se lit : «&nbsp;$A$ union $B$ est l'ensemble des éléments $x$ tels que $x$ est un élément de l'ensemble $A$ **ou** x est un élément de l'ensemble $B$&nbsp;»

#### Propriétés

Quels que soient les ensembles $A, B$ et $C$, on a toujours :

* l'union est **commutative** : $A \cup B = B \cup A$
* l'union est **associative** : $A \cup ( B \cup C ) = (A \cup B) \cup C = A \cup B \cup C$.<br>Cela signifie que l'expression $A \cup B \cup C$ est sans ambiguité.
* $A \cup A = A$
* $A \cup \varnothing = A$
* $A \cup \Omega = \Omega$
  
### Intersection

L'intersection de deux ensembles $A$ et $B$ est l'ensemble des éléments qui appartiennent **à la fois** à $A$ **et à** $B$. On note cet ensemble $A \cap B$. La définition est : 

$$A \cap B = \{x | x\in A \land x \in B\}$$

#### Propriétés

Quels que soient les ensembles $A, B$ et $C$, on a toujours :

* l'intersection est **commutative** : $A \cap B = B \cap A$
* l'intersection est **associative** : $A \cap ( B \cap C ) = (A \cap B) \cap C = A \cap B \cap C$.<br>Cela signifie que l'expression $A \cap B \cap C$ est sans ambiguité.
* $A \cap A = A$
* $A \cap \varnothing = \varnothing$
* $A \cap \Omega = A$

### Différence

Si $A$ et $B$ sont deux ensembles, on appelle l'ensemble $A$ privé de $B$ l'ensemble dont les éléments appartiennent à $A$ mais pas à $B$. La définition est :

$$A \setminus B = \{x|x\in A \land x \notin B\}$$

#### Propriété

Quels que soient les ensembles $A, b$ et l'ensemble univers $\Omega$, on a toujours :

* $A \setminus \varnothing = A$
* $\varnothing \setminus A = \varnothing$
* $A \setminus \Omega = \varnothing$
* $\Omega \setminus A = \complement_\Omega A$
* $A \subset B \implies A \setminus B = \varnothing$

### Complémentaire

Si $A$ est un ensemble, $B$ un sous-ensemble de $A$, on appelle complémentaire de $B$ dans $A$ tous les éléments de $A$ qui n'appartiennent pas à $B$.On le note $\complement_A B$. La définition est :

$$B \subset A, \complement_A B = \{x|x\in A \land x\notin B\}$$

Si l'ensemble considéré est un ensemble univers, on note simplement : $\complement_\Omega A = \complement A$. Dans un contexte probaviliste on note également : $\complement_\Omega B= \overline{B\ }$

#### Propriétés

Quels que soient les ensembles $A,B$ et $C$ avec $B \subset A$ et $C \subset A$ on a :

* $\complement_A \varnothing = A$
* $\complement_A A = \varnothing$
* $\complement_A (\complement_A B) = B$
* $B \subset C \implies \complement_A C \subset \complement_A B$

### Propriétés sur les opérateurs

Quels que soient les ensembles $A, B$ et $C$, on a :

* $A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$
* $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$
* $(A \cap B) \setminus C = A \cap (B \setminus C) = (A \setminus C) \cap (B \setminus C)$
* $A \setminus (B \cap C) = (A \setminus B) \cup (A \setminus C)$
* $\complement_A (B \cup C) = \complement_A B \cap \complement_A C$
* $\complement_A (B \cap C) = \complement_A B \cup \complement_A C$
* $|A \cup B| = |A| + |B| - |A \cap B|$
* $|A \cap B| = |A| + |B| - |A \cup B|$
* $|A \setminus B| = |A| - |A \cap B|$
* $|\complement_A B|=|A|-|B|$


## Cheat sheet

Soient l'ensemble univers $\Omega$ ainsi que deux ensembles $A$ et $B$ :

<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   version="1.1"
   id="svg1"
   xml:space="preserve"
   width="188.70657"
   height="130.14384"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"><defs
     id="defs1"><rect
       x="243.48965"
       y="509.62952"
       width="96.263351"
       height="99.094627"
       id="rect15-2" /><rect
       x="431.29758"
       y="476.59799"
       width="177.42657"
       height="151.00134"
       id="rect16-4" /></defs><sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1" /><inkscape:clipboard
     style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:#000000;fill-opacity:1;stroke-width:1.0591183;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;-inkscape-stroke:none;stop-color:#000000;stop-opacity:1"
     min="16.58342,20.239926"
     max="205.28999,150.38377"
     geom-min="16.58342,20.846113"
     geom-max="204.77216,150.38377" /><g
     id="g1"
     transform="matrix(3.7795276,0,0,3.7795276,-16.58342,-20.239927)"><g
       id="g15-5"
       transform="matrix(0.25900073,0,0,0.30318864,2.6347674,1.2759403)"><g
         id="g11-1"><rect
           style="fill:#ffffff;stroke:#aa0088;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
           id="rect10-1"
           width="184.53032"
           height="93.638527"
           x="14.482758"
           y="13.983353" /><text
           xml:space="preserve"
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;text-anchor:start;fill:#aa0088;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
           x="5.8270073"
           y="127.1"
           id="text10-0"
           transform="scale(1.0005758,0.99942453)"><tspan
             sodipodi:role="line"
             id="tspan10-8"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';stroke-width:0;stroke-linecap:round;stroke-dasharray:none"
             x="5.8270073"
             y="127.1">Ω</tspan></text></g><g
         id="g14-5"><g
           id="g12-0"><ellipse
             style="fill:none;stroke:#0000aa;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="ellipse11-6"
             cx="80.529129"
             cy="67.045181"
             rx="39.078476"
             ry="34.334126" /><text
             xml:space="preserve"
             transform="matrix(0.75242193,0,0,0.69764464,-147.27799,-332.27374)"
             id="text11-4"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect15-2);display:inline;fill:#0000aa;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
               x="243.49023"
               y="536.05101"
               id="tspan1">A</tspan></text></g><g
           id="g13-25"><ellipse
             style="fill:none;stroke:#aa0000;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="ellipse12-8"
             cx="135.71344"
             cy="60.053509"
             rx="41.076099"
             ry="32.586208" /><text
             xml:space="preserve"
             transform="matrix(0.80839028,0,0,0.67289828,-178.16944,-297.5308)"
             id="text12-6"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect16-4);display:inline;fill:#aa0000;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
               x="431.29688"
               y="503.01976"
               id="tspan2">B</tspan></text></g></g></g></g></svg>

On aura :

$A \cup B$

<svg
   version="1.1"
   id="svg1"
   xml:space="preserve"
   width="188.70657"
   height="130.14384"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"><defs
     id="defs1"><rect
       x="243.48965"
       y="509.62952"
       width="96.263351"
       height="99.094627"
       id="rect5" /><rect
       x="431.29758"
       y="476.59799"
       width="177.42657"
       height="151.00134"
       id="rect6" /></defs><sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1" /><inkscape:clipboard
     style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:#37c837;fill-opacity:1;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;-inkscape-stroke:none;stop-color:#000000;stop-opacity:1;stroke:#aa0000"
     min="16.58342,166.47089"
     max="205.28999,296.61473"
     geom-min="16.58342,167.07707"
     geom-max="204.77216,296.61473" /><g
     id="g1"
     transform="matrix(3.7795276,0,0,3.7795276,-16.58342,-166.47089)"><g
       id="g34"
       transform="translate(-2.2473246,31.848161)"><g
         id="g10"
         transform="matrix(0.25900073,0,0,0.30318864,4.882092,8.118054)"><g
           id="g8"><rect
             style="fill:#ffffff;stroke:#aa0088;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="rect1"
             width="184.53032"
             height="93.638527"
             x="14.482758"
             y="13.983353" /><text
             xml:space="preserve"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;text-anchor:start;fill:#aa0088;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
             x="5.8270073"
             y="127.1"
             id="text4"
             transform="scale(1.0005758,0.99942453)"><tspan
               sodipodi:role="line"
               id="tspan4"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';stroke-width:0;stroke-linecap:round;stroke-dasharray:none"
               x="5.8270073"
               y="127.1">Ω</tspan></text></g><g
           id="g9"><g
             id="g6"><ellipse
               style="fill:none;stroke:#0000aa;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="path1"
               cx="80.529129"
               cy="67.045181"
               rx="39.078476"
               ry="34.334126" /><text
               xml:space="preserve"
               transform="matrix(0.75242193,0,0,0.69764464,-147.27799,-332.27374)"
               id="text5"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect5);display:inline;fill:#0000aa;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="243.49023"
                 y="536.05101"
                 id="tspan1">A</tspan></text></g><g
             id="g7"><ellipse
               style="fill:none;stroke:#aa0000;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="path2"
               cx="135.71344"
               cy="60.053509"
               rx="41.076099"
               ry="32.586208" /><text
               xml:space="preserve"
               transform="matrix(0.80839028,0,0,0.67289828,-178.16944,-297.5308)"
               id="text6"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect6);display:inline;fill:#aa0000;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="431.29688"
                 y="503.01976"
                 id="tspan2">B</tspan></text></g></g></g><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 90.600803,145.36118 c -8.179125,-1.76552 -13.545891,-4.73029 -19.600397,-10.82786 -3.388223,-3.41233 -4.712111,-5.16719 -6.443879,-8.54158 -3.678807,-7.16823 -4.392568,-10.16171 -4.388056,-18.40329 0.0044,-8.049732 0.56336,-10.706047 3.574618,-16.987647 9.576612,-19.977179 33.261533,-27.422967 52.186151,-16.40567 l 2.91418,1.696539 -1.78713,2.635774 c -2.48462,3.664498 -3.99937,6.785398 -5.29309,10.905562 -1.44157,4.59104 -1.57423,14.493312 -0.25902,19.335142 1.91464,7.04856 6.86967,14.62711 12.60005,19.27134 1.42743,1.15687 2.6667,2.28599 2.75394,2.50914 0.29174,0.74631 -3.6721,4.99706 -7.12843,7.6444 -3.75485,2.87598 -8.93772,5.46108 -13.32098,6.6442 -3.99476,1.07825 -12.009404,1.3439 -15.807957,0.52395 z"
         id="path22"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 125.41519,126.94165 c -10.1406,-7.97176 -15.33815,-21.56219 -12.91205,-33.762093 0.82981,-4.172782 4.38939,-12.165266 6.58882,-14.794182 l 1.13967,-1.362207 2.46717,2.305965 c 4.91437,4.59326 8.97546,11.615964 10.88423,18.821737 1.39479,5.26542 1.39479,13.60974 0,18.87517 -0.95998,3.62401 -3.47817,9.47458 -4.82488,11.20975 -0.59684,0.769 -0.8366,0.67618 -3.34296,-1.29414 z"
         id="path23"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 145.90168,135.659 c -4.7748,-0.6647 -12.218,-3.11159 -14.98983,-4.92776 l -1.04387,-0.68397 2.01602,-4.01697 c 3.08876,-6.15441 4.26326,-11.45387 4.19177,-18.91373 -0.10952,-11.429014 -3.59721,-20.029726 -11.43562,-28.200426 l -3.06106,-3.190835 1.38054,-1.458016 c 3.60624,-3.80863 10.40607,-7.674561 17.04853,-9.692668 3.65073,-1.10916 4.97544,-1.26448 10.99318,-1.288926 5.20513,-0.02114 7.61408,0.17853 10.0681,0.834532 14.7944,3.954794 25.32137,14.098507 28.69554,27.650825 1.37346,5.516521 1.07718,12.747114 -0.74482,18.176794 -4.08186,12.16419 -14.67905,21.65078 -27.64477,24.74756 -4.63557,1.10718 -11.41133,1.52912 -15.47371,0.96359 z"
         id="path24"
         transform="scale(0.26458333)" /></g></g></svg>

$A \cap B$
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   version="1.1"
   id="svg1"
   xml:space="preserve"
   width="188.70657"
   height="130.14384"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"><defs
     id="defs1"><rect
       x="243.48965"
       y="509.62952"
       width="96.263351"
       height="99.094627"
       id="rect15" /><rect
       x="431.29758"
       y="476.59799"
       width="177.42657"
       height="151.00134"
       id="rect16" /></defs><sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1" /><inkscape:clipboard
     style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:#37c837;fill-opacity:1;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;-inkscape-stroke:none;stop-color:#000000;stop-opacity:1;stroke:#aa0000"
     min="16.58342,312.70184"
     max="205.28999,442.84569"
     geom-min="16.58342,313.30803"
     geom-max="204.77216,442.84569" /><g
     id="g1"
     transform="matrix(3.7795276,0,0,3.7795276,-16.58342,-312.70185)"><g
       id="g35"
       transform="translate(-2.2473246,32.135792)"><g
         id="g15"
         transform="matrix(0.25900073,0,0,0.30318864,4.882092,46.520697)"><g
           id="g11"><rect
             style="fill:#ffffff;stroke:#aa0088;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="rect10"
             width="184.53032"
             height="93.638527"
             x="14.482758"
             y="13.983353" /><text
             xml:space="preserve"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;text-anchor:start;fill:#aa0088;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
             x="5.8270073"
             y="127.1"
             id="text10"
             transform="scale(1.0005758,0.99942453)"><tspan
               sodipodi:role="line"
               id="tspan10"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';stroke-width:0;stroke-linecap:round;stroke-dasharray:none"
               x="5.8270073"
               y="127.1">Ω</tspan></text></g><g
           id="g14"><g
             id="g12"><ellipse
               style="fill:none;stroke:#0000aa;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="ellipse11"
               cx="80.529129"
               cy="67.045181"
               rx="39.078476"
               ry="34.334126" /><text
               xml:space="preserve"
               transform="matrix(0.75242193,0,0,0.69764464,-147.27799,-332.27374)"
               id="text11"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect15);display:inline;fill:#0000aa;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="243.49023"
                 y="536.05101"
                 id="tspan1">A</tspan></text></g><g
             id="g13"><ellipse
               style="fill:none;stroke:#aa0000;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="ellipse12"
               cx="135.71344"
               cy="60.053509"
               rx="41.076099"
               ry="32.586208" /><text
               xml:space="preserve"
               transform="matrix(0.80839028,0,0,0.67289828,-178.16944,-297.5308)"
               id="text12"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect16);display:inline;fill:#aa0000;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="431.29688"
                 y="503.01976"
                 id="tspan2">B</tspan></text></g></g></g><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 124.8688,271.57592 c -6.97546,-6.10407 -10.6261,-12.24467 -12.35646,-20.7843 -0.91778,-4.52945 -0.72733,-10.2096 0.49796,-14.85203 0.91135,-3.45293 4.01046,-9.78768 5.8868,-12.03292 l 1.38022,-1.65157 2.66282,2.59533 c 5.26965,5.13611 8.77368,11.15662 10.68534,18.35922 1.35451,5.10341 1.34779,13.02595 -0.0158,18.61977 -1.10533,4.53433 -4.52528,12.09645 -5.47059,12.09645 -0.32167,0 -1.7933,-1.05748 -3.27028,-2.34995 z"
         id="path25"
         transform="scale(0.26458333)" /></g></g></svg>

$A \setminus B$

<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   version="1.1"
   id="svg1"
   xml:space="preserve"
   width="188.70657"
   height="130.14384"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"><defs
     id="defs1"><rect
       x="243.48965"
       y="509.62952"
       width="96.263351"
       height="99.094627"
       id="rect15-8" /><rect
       x="431.29758"
       y="476.59799"
       width="177.42657"
       height="151.00134"
       id="rect16-8" /></defs><sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1" /><inkscape:clipboard
     style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:#37c837;fill-opacity:1;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;-inkscape-stroke:none;stop-color:#000000;stop-opacity:1;stroke:#aa0000"
     min="16.58342,458.9328"
     max="205.28999,589.07665"
     geom-min="16.58342,459.53899"
     geom-max="204.77216,589.07665" /><g
     id="g1"
     transform="matrix(3.7795276,0,0,3.7795276,-16.58342,-458.93281)"><g
       id="g36"
       transform="translate(-2.2473246,32.423424)"><g
         id="g15-7"
         transform="matrix(0.25900073,0,0,0.30318864,4.882092,84.92334)"><g
           id="g11-7"><rect
             style="fill:#ffffff;stroke:#aa0088;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="rect10-6"
             width="184.53032"
             height="93.638527"
             x="14.482758"
             y="13.983353" /><text
             xml:space="preserve"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;text-anchor:start;fill:#aa0088;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
             x="5.8270073"
             y="127.1"
             id="text10-4"
             transform="scale(1.0005758,0.99942453)"><tspan
               sodipodi:role="line"
               id="tspan10-3"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';stroke-width:0;stroke-linecap:round;stroke-dasharray:none"
               x="5.8270073"
               y="127.1">Ω</tspan></text></g><g
           id="g14-0"><g
             id="g12-3"><ellipse
               style="fill:none;stroke:#0000aa;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="ellipse11-0"
               cx="80.529129"
               cy="67.045181"
               rx="39.078476"
               ry="34.334126" /><text
               xml:space="preserve"
               transform="matrix(0.75242193,0,0,0.69764464,-147.27799,-332.27374)"
               id="text11-9"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect15-8);display:inline;fill:#0000aa;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="243.49023"
                 y="536.05101"
                 id="tspan3">A</tspan></text></g><g
             id="g13-2"><ellipse
               style="fill:none;stroke:#aa0000;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="ellipse12-5"
               cx="135.71344"
               cy="60.053509"
               rx="41.076099"
               ry="32.586208" /><text
               xml:space="preserve"
               transform="matrix(0.80839028,0,0,0.67289828,-178.16944,-297.5308)"
               id="text12-4"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect16-8);display:inline;fill:#aa0000;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="431.29688"
                 y="503.01976"
                 id="tspan4">B</tspan></text></g></g></g><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 90.128924,435.5369 c -13.267433,-2.71091 -23.721232,-12.1752 -28.397848,-25.70983 -1.499366,-4.33933 -1.548958,-4.72458 -1.548958,-12.03292 0,-7.30834 0.04959,-7.69359 1.548958,-12.03292 4.386974,-12.69637 14.110191,-21.99313 26.510331,-25.34759 3.734605,-1.01027 14.190713,-1.12213 17.459533,-0.18677 5.23628,1.49833 12.74074,5.15424 12.74074,6.20684 0,0.22575 -0.83667,1.60482 -1.85926,3.06462 -5.17781,7.39157 -7.4006,18.60312 -5.48243,27.6529 1.8349,8.6569 7.76854,17.66475 15.06347,22.86783 l 1.35143,0.96389 -3.47488,3.56545 c -9.16744,9.40637 -21.7306,13.47733 -33.911086,10.9885 z"
         id="path26"
         transform="scale(0.26458333)" /></g></g></svg>

$\overline{A\ }=\complement A$

<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   version="1.1"
   id="svg1"
   xml:space="preserve"
   width="188.70657"
   height="130.14384"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg"><defs
     id="defs1"><rect
       x="243.48965"
       y="509.62952"
       width="96.263351"
       height="99.094627"
       id="rect5-9" /><rect
       x="431.29758"
       y="476.59799"
       width="177.42657"
       height="151.00134"
       id="rect6-2" /></defs><sodipodi:namedview
     id="namedview1"
     pagecolor="#ffffff"
     bordercolor="#000000"
     borderopacity="0.25"
     inkscape:showpageshadow="2"
     inkscape:pageopacity="0.0"
     inkscape:pagecheckerboard="0"
     inkscape:deskcolor="#d1d1d1" /><inkscape:clipboard
     style="font-variation-settings:normal;opacity:1;vector-effect:none;fill:#37c837;fill-opacity:1;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;-inkscape-stroke:none;stop-color:#000000;stop-opacity:1;stroke:#aa0000"
     min="16.58342,605.16376"
     max="205.28999,735.30761"
     geom-min="16.58342,605.76995"
     geom-max="204.77216,735.30761" /><g
     id="g1"
     transform="matrix(3.7795276,0,0,3.7795276,-16.58342,-605.16377)"><g
       id="g37"
       transform="translate(-2.2473246,32.711058)"><g
         id="g10-4"
         transform="matrix(0.25900073,0,0,0.30318864,4.882092,123.32598)"><g
           id="g8-8"><rect
             style="fill:#ffffff;stroke:#aa0088;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
             id="rect1-1"
             width="184.53032"
             height="93.638527"
             x="14.482758"
             y="13.983353" /><text
             xml:space="preserve"
             style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;text-anchor:start;fill:#aa0088;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
             x="5.8270073"
             y="127.1"
             id="text4-2"
             transform="scale(1.0005758,0.99942453)"><tspan
               sodipodi:role="line"
               id="tspan4-8"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:22.0606px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';stroke-width:0;stroke-linecap:round;stroke-dasharray:none"
               x="5.8270073"
               y="127.1">Ω</tspan></text></g><g
           id="g9-9"><g
             id="g6-3"><ellipse
               style="fill:none;stroke:#0000aa;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="path1-6"
               cx="80.529129"
               cy="67.045181"
               rx="39.078476"
               ry="34.334126" /><text
               xml:space="preserve"
               transform="matrix(0.75242193,0,0,0.69764464,-147.27799,-332.27374)"
               id="text5-8"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect5-9);display:inline;fill:#0000aa;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="243.49023"
                 y="536.05101"
                 id="tspan1">A</tspan></text></g><g
             id="g7-0"><ellipse
               style="fill:none;stroke:#aa0000;stroke-width:1.058;stroke-linejoin:round;stroke-dasharray:none"
               id="path2-2"
               cx="135.71344"
               cy="60.053509"
               rx="41.076099"
               ry="32.586208" /><text
               xml:space="preserve"
               transform="matrix(0.80839028,0,0,0.67289828,-178.16944,-297.5308)"
               id="text6-1"
               style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:29.3333px;font-family:'Liberation Sans';-inkscape-font-specification:'Liberation Sans';text-align:start;writing-mode:lr-tb;direction:ltr;white-space:pre;shape-inside:url(#rect6-2);display:inline;fill:#aa0000;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"><tspan
                 x="431.29688"
                 y="503.01976"
                 id="tspan2">B</tspan></text></g></g></g><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 33.503422,535.81881 v -52.85046 h 89.421108 89.4211 v 52.85046 52.85047 h -89.4211 -89.421108 z m 71.597858,46.44278 c 8.55492,-1.76707 16.65244,-6.72182 22.13502,-13.54409 l 1.43402,-1.78444 3.96935,1.89761 c 6.62542,3.1674 10.35427,3.95059 18.83355,3.9557 6.56706,0.004 7.73049,-0.12276 11.39049,-1.24068 5.41821,-1.65494 8.68253,-3.13674 12.28604,-5.57709 10.97618,-7.43323 16.90508,-18.33825 16.90508,-31.09354 0,-11.4802 -4.92199,-21.58929 -14.05801,-28.87322 -8.68767,-6.92646 -20.99205,-10.36211 -31.90815,-8.90945 -9.38049,1.24831 -17.5141,4.97563 -23.96007,10.97997 -1.98185,1.84607 -2.30044,1.99178 -3.06721,1.40275 -1.71846,-1.32008 -9.4307,-4.80691 -12.41669,-5.61379 -4.25848,-1.15072 -14.59476,-1.12062 -18.875172,0.055 -14.502376,3.98303 -25.443714,15.64693 -28.58741,30.47533 -1.020964,4.81576 -0.86364,13.68244 0.327238,18.44296 1.99835,7.98838 5.803898,14.42937 11.811749,19.99173 5.146067,4.76447 11.347206,8.02013 18.056687,9.47995 3.982632,0.86652 11.414328,0.8454 15.723488,-0.0447 z m -48.239838,-70.50861 0.783665,-2.3594 h 3.227308 3.227307 l 0.783666,2.3594 c 0.652764,1.96528 0.974385,2.35939 1.92544,2.35939 0.627976,0 1.141775,-0.23883 1.141775,-0.53074 0,-0.29191 -0.831361,-2.78697 -1.847468,-5.54458 -4.120519,-11.18266 -3.749844,-10.44045 -5.214138,-10.44045 -0.827784,0 -1.455824,0.3123 -1.660648,0.82579 -0.392609,0.98425 -2.427608,6.3973 -4.434998,11.79698 l -1.447265,3.893 h 1.365845 c 1.219939,0 1.44956,-0.25204 2.149511,-2.35939 z m 141.527038,0.0357 c 1.74355,-1.63973 1.72707,-4.75621 -0.0325,-6.14026 -1.17014,-0.92044 -1.19989,-1.0365 -0.47188,-1.84095 0.87039,-0.96176 1.02545,-3.4493 0.29957,-4.80562 -0.6538,-1.22165 -3.30971,-1.87138 -7.66248,-1.87451 l -3.65706,-0.003 v 8.0637 8.06369 l 5.15395,-0.15972 c 4.68972,-0.14533 5.26352,-0.26276 6.37037,-1.3037 z"
         id="path27"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 142.50751,570.47641 c -3.63972,-0.78119 -6.33142,-1.74599 -10.0595,-3.60569 l -2.74538,-1.36948 1.46395,-2.59399 c 3.47316,-6.1541 5.55297,-15.63685 4.99052,-22.75397 -0.79805,-10.09831 -4.61541,-18.61582 -11.51016,-25.68215 l -3.10311,-3.18034 1.87004,-1.93136 c 2.6534,-2.7404 8.31976,-6.35252 12.47572,-7.95288 20.14946,-7.75904 42.85074,0.65494 51.46836,19.07619 9.64616,20.61989 -2.84484,44.10093 -26.47308,49.76511 -4.76133,1.14138 -13.61148,1.25146 -18.37736,0.22856 z"
         id="path28"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 189.69543,501.60757 v -2.12345 h 2.39042 c 2.80446,0 4.21589,0.62619 4.21589,1.87041 0,1.60953 -1.30533,2.3765 -4.04468,2.3765 h -2.56163 z"
         id="path29"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 189.69543,508.68576 v -2.59533 h 2.99408 c 2.37428,0 3.16732,0.19143 3.83096,0.92474 1.96987,2.17668 -0.36723,4.26593 -4.772,4.26593 h -2.05304 z"
         id="path31"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 59.595732,503.96697 c 0.548474,-1.68697 1.122981,-3.06722 1.276683,-3.06722 0.153701,0 0.728208,1.38025 1.276682,3.06722 l 0.997225,3.06721 h -2.273907 -2.273908 z"
         id="path33"
         transform="scale(0.26458333)" /><path
         style="fill:#37c837;fill-opacity:1;stroke:#aa0000;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-dasharray:none"
         d="m 59.136694,506.37548 c 0.139022,-0.36229 0.513871,-1.53019 0.832997,-2.59533 0.319127,-1.06515 0.725352,-1.93664 0.902724,-1.93664 0.177371,0 0.583597,0.87149 0.902723,1.93664 0.319126,1.06514 0.693975,2.23304 0.832997,2.59533 0.191662,0.49946 -0.227942,0.6587 -1.73572,0.6587 -1.507778,0 -1.927383,-0.15924 -1.735721,-0.6587 z"
         id="path34"
         transform="scale(0.26458333)" /></g></g></svg>
