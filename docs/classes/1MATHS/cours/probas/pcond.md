---
title: Probabilités conditionnelles
---

## Conditionnel ? Késako ?

Si l'on recherche une définition de l'adjectif **conditionnel** on peut par exemple trouver sur le [wiktionnaire](https://fr.wiktionary.org/wiki/conditionnel){:target="_blank"} :

!!! note "Définition du wiktionnaire"
    **conditionnel** \kɔ̃.di.sjɔ.nɛl\

    **Qui est soumis à certaines conditions ou subordonné à quelque événement incertain.**


    ex: *Si des étrangers sont punis de l’expulsion judiciaire exécutoire après subissement de la peine privative de liberté, il ne reste pas de place pour la libération conditionnelle avec patronage ; […]. — (Actes du Congrès pénitentiaire international de Londres, 1925 Commission internationale pénale et pénitentiaire, Berne, 1927)*

    ex: *Cette promesse n’est pas pure et simple, elle est conditionnelle.*

## Probabilité conditionnelle

Vous savez ce qu'est une probabilité dans les cas simples étudiés en seconde : nous avons un ensemble d'issues élémentaires,
à chacune est associée une probabilité de réalisation et nous avons des règles de calcul permettant ensuite de calculer la
probabilité de réalisation d'un ensemble d'issues élémentaires (un évènement).

Nous allons dans ce chapitre apprendre comment calculer la probabilité d'un évènement lorsqu'on possède une **information**
supplémentaire qui influe sur la réalisation de l'évènement.

### Exemple

Essayons de déterminer, par exemple, la probabilité que Hari possède des yeux bleus. **A priori** sans aucune autre information,
nous sommes obligé de recourir aux statistiques mondiales. Le World Atlas[^1] nous informe qu'environ 8% des humains vivant actuellement
ont des yeux bleus. On peut donc prendre, a priori, une probabilité que Hari a des yeux bleus de 8%.<br/>
Utilisons une notation mathématique usuelle pour décrire ce problème :

Soit $\Omega$ l'ensemble univers contenant toutes les issues possibles quant à la couleur des yeux d'un humain : $\Omega$ = 
{ "deux yeux bruns", "deux yeux bleus", "deux yeux verts", "un œil bleu et un œil vert", … }. Nommons $HB$ l'événement «&nbsp;
Hari a deux yeux bleus&nbsp;». Nous avons a priori $P(HB)=8\%$.

Cette probabilité change-t-elle si nous obtenons **l'information supplémentaire** : «&nbsp;Hari habite en France&nbsp;» ?<br/>
Oui, car d'autres statistiques[^2] indiquent qu'en France environ 22% de la population possède des yeux bleus. Donc, **sachant que**
Hari habite en France, nous pouvons estimer sa probilité d'avoir les yeux bleus est **maintenant** (on dit aussi *a posteriori*)
de 22%. Si on note $HFR$ l'information (ou évènement réalisé) «&nbsp;Hari habite de France&nbsp;», la notation mathématique est :
$P_{HFR}(HB)=22\%$. Cela se lit «&nbsp;La probabilité de HB sachant HFR est de 22%&nbsp;».

### Activité

En reprenant l'exemple précédent, comment évoluerait la probabilité $P(HB)$ en sachant que les deux parents de Hari ont eux-aussi
leurs deux yeux bleus ?

### Remarque sur les notations

Il existe deux manières de noter la «&nbsp;probabilité de $B$ sachant $A$&nbsp;»&nbsp;:

* $P(B|A)$
* $P_A(B)$

Ces deux notations sont équivalentes et signifient **exactement** la même chose. Comme à l'accoutumée, toutes les notations utilisées dans un ouvrage particulier seront toujours clairement établies en début d'ouvrage.<br/>
La première notation a l'avantage d'être explicite, mais le désavantage de laisser croire que c'est l'évènement $B$ qui serait conditionné par l'évènement $A$ ce qui n'a, ici, aucun sens. Ce sont **les probabilités** qui sont conditionnelles **pas les évènements**.<br/>
La seconde notation montre plus clairement que l'on cherche une **probabilité conditionnée par la réalisation d'un évènement** $A$, le symbole $P_A$.<br/>
Ces deux notations sont usuelles. Vous pouvez indifférement utiliser l'une ou l'autre, tout en restant cohérent : ne changez pas de notations en cours de route … <ins>utilisez toujours la même dans un exercice</ins>.


## Formule des probabilités conditionnelles

Spot $P$ une probabilité définie sur un univers $\Omega$.

!!! definition "Définition"

    Soit $A$ un évènement de probabilité non nulle.<br/>
    On appelle **probabilité conditionnelle sachant** $A$ l'application notée $P_A$ qui à tout évènement $B$ associe le réel :

    $$P_A(B)=\frac{P(A \cap B)}{P(A)}$$

!!! note "Corollaire"

    On a donc aussi :
    
    $$P(A \cap B) = P(A) \times P_A(B)$$

### Propriétés

Une probabilité conditionnelle **est** une probabilité. Elle en a donc toutes les propriétés : <br/>

* $P_A(\bar B)=1-P_A(B)$
* $P_A(B) \in [0;1]$
* $B \subset C \implies P_A(B)≤P_A(C)$
* $B \cap C=\varnothing \implies P_A(B \cup C)=P_A(B)+P_A(C)$


## Système complet d'évènements

Soient $n$ évènements $A_1$, $A_2$, $A_3$, …, $A_n$ d'un univers $\Omega$.

Ces évènements forment une **partition** de l'univers $\Omega$ **si et seulement si** :

* ils sont deux à deux disjoints ;
* leur réunion est égale à l'univers $\Omega$.

On dit que ($A_1$, $A_2$, $A_3$, …, $A_n$) est un **système complet d'évènements**.


## Formule des probabilités totales

Soit ($A_1$, $A_2$, $A_3$, …, $A_n$) un système complet d'évènements vérifiant $P(A_i)≠0$ pour tout $i\in {0,1,2,…,n}$.<br/>
Alors pour tout évènement $B$ on a :

$$P(B) = P(A_1)P_{A_1}(B) + P(A_2)P_{A_2}(B) +  … P(A_n)P_{A_n}(B)$$

Que l'on note plus succintement en :

$$P(B) = \sum_{i=1}^nP(A_i)P_{A_i}(B)$$

!!! note "Corollaire"

    Cela est en particulier vrai pour tout évènement $A$ de l'univers $\Omega$ et on obtient pour tout évènement $B$ :

    $$ P(B) = P(A)P_A(B) + P(\bar A)P_{\bar A}P(B) $$

    Car par définition $(A,\bar A)$ est un système complet à deux évènements.

## Évènements indépendants

On dit que deux évènements $A$ et $B$ sont **indépendants** si et seulement si $P(A \cap B)=P(A)\times P(B)$.

Lorsque $P(A)≠0$, $A$ et $B$ sont indépendants si et seulement si $P_A(B)=P(B)$.


!!! warning "ATTENTION"

    L'indépendance n'est acquise que dans les cas suivants :

      * lancers successifs d'un dé ;
      * lancers successifs d'une pièce ;
      * tirages avec remise dans une urne ;
      * tirages avec remise dans un jeu de carte.

    Dans tous les autres cas, l'indépendance **<ins>doit être démontrée grâce à la définition</ins>**.


### Propriété

Sideux évènements $A$ et $B$ sont indépendants alors $\bar A$ et $B$, $A$ et $\bar B$, $\bar A$ et $\bar B$ le seront aussi.

---

Sources:


[Couleurs des yeux et hérédité : Comment ça fonctionne ?](https://www.eyes-road.com/couleurs-des-yeux-et-heredite-comment-ca-fonctionne-10933/){:target="_blank"}

[Page wikipedia sur la couleur des yeux](https://fr.wikipedia.org/wiki/Couleur_des_yeux#cite_note-27){:target="_blank"}

[^1]: [World Atlas : The world's population by eye color](https://www.worldatlas.com/society/the-world-s-population-by-eye-color.html){:target="_blank"}

[^2]: [World Population Review : Eye Color Percentage by Country 2024](https://worldpopulationreview.com/country-rankings/eye-color-percentage-by-country){:target="_blank"}