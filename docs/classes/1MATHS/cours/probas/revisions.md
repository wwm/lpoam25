---
title: Probabilités (Rappels)
---

Un condensé de tout ce que vous avez appris en seconde …

## Terminologie

* une **expérience aléatoire** est une expérience pouvant comporter plusieurs résultats possibles qu'on ne peut prévoir ;
* chaque résultat possible de cette expérience s'appelle une **éventualité** ou encore **un cas possible** ou encore **une issue** ;
* l'ensemble de toutes les éventualités s'appelle **l'univers des possibles** ou **ensemble univers**, on le note $\Omega$. Il sera toujours supposé **fini** et **non vide** ;
* une partie de l'ensemble $\Omega$, un de ses sous-ensemble, s'appelle un **évènement**.

## Vocabulaire et liens avec la théorie des ensembles

Le domaine des probabilités utilise les notions usuelles des mathématiques mais avec un vocabulaire légèrement différent. Voici un petit lexique qui traduit en langage des probabilités certains termes mathématiques usuels :

| Notion                   | Théorie des ensembles     | Probabilités                             |
| ------------------------ | ------------------------- | ---------------------------------------- |
| $\Omega$                 | ensemble univers          | ensemble des possibles                   |
| $\omega$                 | un élément de $\Omega$    | une éventualité, une issue               |
| $A\subset \Omega$        | un sous-ensemble $A$      | un évènement $A$                         |
| $\omega\in A$            | $\omega$ appartient à $A$ | l'issue $\omega$ réalise l'évènement $A$ |
| $\varnothing             | ensemble vide             | évènement impossible                     |
| $\Omega$                 | ensemble univers          | évènement certain                        |
| $\{\omega\}$             | un singleton              | un évènement élémentaire                 |
| $\bar A$                 | le complémentaire de $A$  | l'évènement contraire de $A$             |
| $A \subset B$            | $A$ est inclus dans $B$   | l'évènement $A$ implique l'évènement $B$ |
| $A \cap B$               | $A$ inter $B$             | $A$ et $B$                               |
| $A \cup B$               | $A$ union $B$             | $A$ ou $B$                               |
| $A \cap B = \varnothing$ | $A$ et $B$ sont disjoints | $A$ et $B$ sont incompatibles            |

## Loi de probabilité

### Définition

Une **loi de probabilité** sur un univers $\Omega=\{\omega_1, \omega_2, …, \omega_n\}$ est une application $p:\Omega\rightarrow\mathbb{R}$ qui à tout élément $\omega_i$ de $\Omega$ associe une réel $p_i=p(\omega_i)$ et qui possède les deux propriétés suivantes :

$$\left\{
\begin{align}
& p_i≥0 & \forall i \in \{1,2,…,n\} \\
& p_1+p_2+…+p_n=1 &\\ 
\end{align}
\right.$$

### Loi uniforme

Lorsque tous les $p_i$ sont égaux à une même valeur $c$ on dit que la loi est **uniforme**. Dans ce cas cette valeur commune $c$ est égale à $\frac{1}{n}$.

## Probabilité d'un évènement

### Définition

Une loi de probabilité $p$ étant définie sur un univers $\Omega$, on définit **la probabilité d'un évènement $A$** comme étant la somme des $p(\omega)$ pour tous les $\omega$ réalisant $A$. On la note $p(A)$.

### Propriétés

* $p(\Omega)=1$
* $p(\varnothing)=0$

Quels que soient les évènements $A$ et $B$ incompatibles :

* $p(A \cup B) = p(A) + p(B)$

Pour tout évènement $A$ :

* $p(\bar A)=1 -p(A)$

Quels que soient les évènements $A_1, A_2, …, A_n$ deux à deux incompatibles, c'est à dire que pour tous les évènements différents $A_i$ et $A_j$ on a $A_i \cap A_j = \varnothing$, on a :

* $p(A_1\cup A_2\cup … \cup A_n)=p(A_1)+p(A_2)+…+p(A_n)$

Quels que soient les évènements $A$ et $B$ on a :

* $p(A \cup B) = p(A) + p(B) - p(A \cap B)$

## Équiprobabilité

Lorsque la loi esr uniforme, tous les évènements élémentaires $\{\omega\}$ ont la même probabilité $p(\{\omega\})=\frac{1}{n}$. On dit qu'il y a équiprobabilité des évènements élémentaires.<br/>
Dans ce cas, la probabilité d'un évènement $A$ constitué de $k$ éventualités sera égale à $\displaystyle{p(A)=\underbrace{\frac{1}{n}+\frac{1}{n}+…\frac{1}{n}}_{k\times}=\frac{k}{n}}$.<br/>
En nommant «&nbsp;cas favorables&nbsp;» les éventualités qui réalisent $A$, on obtient la formule :

$$p(A) = \frac{\text{nombre de cas favorables}}{\text{nombre de cas possibles}}$$

## Avec les nouvelles notations

Partie écrite du cours …