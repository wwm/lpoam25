---
title: Polynôme du second degré
---

!!! danger " "
    Dans ce chapitre, il s'agit de mettre en place des méthodes de résolution d'équations et d'inéquations du second degré, de définir la notion de polynôme et de résoudre des problèmesse ramenant au second degré.

## Polynôme

### Définitions

!!! definition "Polynôme"

    Un **polynôme** ou une **fonction polynôme** est une fonction de la forme :

    $\begin{align}
        P : \mathbb{R} &\  \rightarrow \mathbb{R} \\
        x &\ \mapsto a_nx^n + a_{n-1}x^{n-1}+\dots+a_1x+a_0
    \end{align}$

    Les réels donnés $a_0, a_1, \dots, a_n$ sont les coefficients de $P$ et $n$ est entier naturel donné.

!!! definition "Polynôme nul"

    le **polynôme nul** est la fonction $x \mapsto 0$ définie sur $\mathbb{R}$ ; tous ses coefficients sont nuls : $a_n=a_{n-1}=\dots=a_1=a_0=0$.

!!! definition "Monôme"

    Un **monôme** est un polynôme n'ayant qu'un seul coefficient non nul, c'est la fonction $x \mapsto a_nx^n$ définie sur $\mathbb{R}$.

!!! definition "Degré d'un polynôme"

    Le **degré** d'un polynôme est le plus grand entier $n$ pour lequel le coefficient $a_n$ n'est pas nul. Le polynôme n'a pas de degré.


!!! definition "Racine d'un polynôme"

    Les réels $r$ tels que $P(r)=0$ sont appelés **racine de $P$**, ou **racines** de l'équation $P(x)=0$.

### Propriétés

!!! definition "Polynôme nul"

    On admettra qu'un polynome est nul **si et seulement si** tous ces coefficients sont nuls.

!!! definition "Égalité de deux polynômes"

    On dit que deux polynomes $P$ et $Q$ **de même degré** sont **égaux** si pour tout réel $x$ on a l'égailté $P(x)=Q(x)$.

    On démontre qu'ils sont égaux **si et seulement si** ils ont mêmes coefficients. Cela signifie que si on a $P: x\mapsto a_nx^n+a_{n-1}x^{n-1}+\dots+a_1x+a_0$ et $Q: x\mapsto b_nx^n+b_{n-1}x^{n-1}+\dots+b_1x+b_0$ avec $a_n \neq 0$ et $b_n \neq 0$, alors $P=Q$ **si et seulement si** $a_n=b_n, a_{n-1}=b_{n-1}, \dots, a_1=b_1, a_0=b_0$


## Trinôme du second degré

### Définition

!!! definition "$\,$"

    Un trinôme du second degré est un polynôme de degré 2, c'est-à-dire une fonction de la forme :

    $\begin{align}
    f : \mathbb{R} &\ \rightarrow \mathbb{R} \\
    x &\ \mapsto ax²+bx+c
    \end{align}$

    avec $a,b$ et $c$ des réels et $a \neq 0$.

### Discriminant Δ

!!! definition "Discriminant d'un trinôme du second degré"

    On appelle **discriminant** d'un trinôme $ax²+bx+c$ le réel $\Delta = b^2-4ac$. 

$\Delta$ est une lettre capitale de l'alphabet grec nommée delta, la bas-de-casse est $\delta$.

### Forme canonique

!!! methode "Mise sous forme canonique"

    Soit $f(x)=ax^2+bx+c$  avec $a \neq 0$. On peut écrire :

    $\begin{align}
        f(x) &= a \left( x^2+ \displaystyle{\frac{b}{a}} x + \frac{c}{a} \right) \\
             &= a \left( x^2+ \displaystyle{2\frac{b}{2a}} x + \frac{c}{a} \right) \\
             &= a \left[ \left( x + \frac{b}{2a} \right)^2 - \left( \frac{b^2}{4a^2}-\frac{c}{a} \right) \right] \\
             &= a \left[ \left( x + \frac{b}{2a} \right)^2 - \frac{b^2-4ac}{4a^2} \right] \\
             &= a \left[ \left( x + \frac{b}{2a} \right)^2 - \frac{\Delta}{4a^2} \right] \\
    \end{align}$

    La dernière expression de $f(x)$ est appelée **forme canonique** du trinôme.

## Racines

!!! definition "Discriminant d'un trinôme du second degré"

    Les racines du trinôme $f(x)=ax^2+bx+c$ avec $a\in\mathbb{R}^*, b\in\mathbb{R}, c\in\mathbb{R}$ sont les solutions de de l'équation $ax^2+bx+c=0$.


!!! methode "Solutions de $ax^2+bx+c=0$"

    0. ne pas hésiter à simplifier si possible
   
        !!! example "par exemple"

            $\begin{align}
            6x^2-12x+54 &= 0 & \text{On remarque une factorisation possible par 6} \\
            6(x^2-2x+9) &= 0 & \text{On simplifie} \\
            x^2-2x+9 &= 0
            \end{align}$
    1. calculer le discrimininant Δ
    2. suivant le signe de Δ :
        - si **Δ<0** alors il n'y a pas de racines réelles.<br>
            $S=\emptyset$<br>

        - si **Δ=0** alors il y une seule racine (on parle de racine double).<br>
            $x_0=x_1=\dfrac{-b}{2a}$<br>
            $S=\left\{x_0\right\}$

        - si **Δ>0** alors il a deux racines.<br>
            $\begin{align}
                x_0 &= \dfrac{-b-\sqrt{\Delta}}{2a} \\
                x_1 &= \dfrac{-b+\sqrt{\Delta}}{2a} \\
            \end{align}$<br>
            $S=\left\{x_0, x_1\right\}$

??? preuve "Preuve"

    Soit le trinôme du second degré :
    $\left|\begin{align}
        f : \mathbb{R} & \rightarrow \mathbb{R} \\
            x & \mapsto ax^2+bx+c
    \end{align}\right.$
    avec $a\in\mathbb{R}^*,b\in\mathbb{R}$ et $c\in\mathbb{R}$<br>

    - Sa forme canonique est : <br>
       $f(x)=a \left[ \left( x + \dfrac{b}{2a} \right)^2 - \dfrac{\Delta}{4a^2} \right]$<br>

    - Résoudre $f(x)=0$ revient donc à résoudre :<br>
        $\begin{align}
            a \left[ \left( x + \frac{b}{2a} \right)^2 - \frac{\Delta}{4a^2} \right] &= 0 & \\
            \left( x + \frac{b}{2a} \right)^2 - \frac{\Delta}{4a^2} &= 0 & \text{car }a\neq 0 \\
            \left( x + \frac{b}{2a} \right)^2 &=  \frac{\Delta}{4a^2} & \\
        \end{align}$<br>

    - Comme $\dfrac{\Delta}{4a^2}$ est du signe de $\Delta$ car $4a^2$ étant le carré de $2a$ est toujours strictment positif, on déduit que :

        1. si $\Delta<0$<br>
           il ne peut y avoir de solution, un carré étant toujours positif ou nul il ne peut être égal à un réel strictement négatif.<br>
           $S=\emptyset$

        1. si $\Delta=0$<br>
            l'équation devient :<br>
            $\begin{align}
                \left( x + \frac{b}{2a} \right)^2 &=  \frac{\Delta}{4a^2} & \\
                \left( x + \frac{b}{2a} \right)^2 &=  0 & \text{car }\Delta=0\\
                x + \frac{b}{2a}  &=  0 & \text{car un produit de facteurs est nul} \\
                 &  &  \text{si et seulement si} \\
                 &  &  \text{l'un des ses facteurs est nul } \\
            \end{align}$<br>
            Nous nous ramenons donc à une équation du premier degré dont la solution unique est $\dfrac{-b}{2a}$.<br>
            $S=\left\{\dfrac{-b}{2a}\right\}$

        1. si $\Delta>0$<br>
           En repartant de :<br>
           $\begin{align}
            \left( x + \frac{b}{2a} \right)^2 - \frac{\Delta}{4a^2} &= 0 & \\
            \left( x + \frac{b}{2a} \right)^2 - \frac{\sqrt{\Delta}^2}{4a^2} &= 0 & \text{en remarquant que } \Delta=\sqrt{\Delta}^2 \text{ car } \Delta>0\\
            \left( x + \frac{b}{2a} \right)^2 - \left(\frac{\sqrt{\Delta}}{2a}\right)^2 &= 0 & \text{identité remarquable !} \\
            \left(x+\dfrac{b}{2a}-\frac{\sqrt{\Delta}}{2a}\right)\left(x+\dfrac{b}{2a}+\frac{\sqrt{\Delta}}{2a}\right) &=0 & \\
            \left(x+\dfrac{b-\sqrt{\Delta}}{2a}\right)\left(x+\dfrac{b+\sqrt{\Delta}}{2a}\right) &=0 & \\
           \end{align}$<br>
           Comme un produit de facteurs est nul si et seulement si au moins l'un de ses facteurs est nul, nous en venons à résoudre deux équations de premier degré :
            1. $x+\dfrac{b-\sqrt{\Delta}}{2a}=0$<br>
                qui donne la solution $x_1=\dfrac{-b+\sqrt{\Delta}}{2a}$
            1. $x+\dfrac{b+\sqrt{\Delta}}{2a}=0$<br>
                qui donne la solution $x_0=\dfrac{-b-\sqrt{\Delta}}{2a}$
          Il y a donc deux racines et : <br>
          $S=\{x_0=\dfrac{-b-\sqrt{\Delta}}{2a},x_1=\dfrac{-b+\sqrt{\Delta}}{2a}\}$<br>
    ■


## Représentation graphique

### Construction de l'intuition

Testez votre intuition sur l'appliquette géogébra ci-dessous, cliquez sur le bouton :material-fullscreen: pour passer en mode plein écran. Pour revenir en mode normal, appuyez sur le bouton :material-fullscreen-exit:<br>
Changez les valeurs des coefficients afin d'apprendre comment ils modifient l'apparence de la parobole.

<!-- <iframe scrolling="no" title="Trinôme second degré - Intuition" src="https://www.geogebra.org/material/iframe/id/ttqgk2w3/width/1034/height/650/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/true/sri/true/rc/true/ld/false/sdz/true/ctl/false" width="1034px" height="650px" style="border:0px;" allowfullscreen> </iframe> -->

??? geogebra "Trinôme second degré - Intuition"
    <div class="container flex justify-center">
        <iframe src="https://www.geogebra.org/calculator/ttqgk2w3?embed" title="Trinôme second degré - Intuition" width="800" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0" class="shadow-splash-min"></iframe>
    </div>

!!! methode "Maths exploratrices"

    Pour aller plus loin, vous pourriez vous poser quelques questions comme :

    - Imaginez ne faire varier que le coefficient $b$, et qu'à chaque variation vous traciez le sommet de la parabole. Quelle courbe décrirait=il à votre avis ? Pouvez-vous le prouver ?
    - En ne faisant varier que le coefficient $c$ et en obervant comment les racines évoluent, pourriez-vous expliquer pourquoi on parle de « racine double » quand $\Delta$ vaut 0 ?
    - Quelles autres conjectures sur le comportement du tracé en fonction des coefficients pourriez-vous formuler ?



## Factorisation et signe du trinôme

- Si Δ<0 alors $f(x)$ est du signe de $a$ pour tout réel $x$.<br>
    $\Delta<0 \implies \forall x\in\mathbb{R}, sgn(f(x))=sgn(a)$

<!-- 
    !!! methode "Représentation graphique"

        Le tableau de signe et de variation en fonction du signe de $a$ donne :

        - $a>0$
            La représentation graphique ressemblera peu ou prou à : <br>![alt text](images/para_a_pos_delta_neg.png){width="200"}<br>
            


        - $a<0$
 -->


- Si Δ=0 alors $f(x)$ est du signe de $a$ pour tout réel $x\neq\dfrac{-b}{2a}$ et de signe nul pour $x=\dfrac{-b}{2a}$<br><br>
      $\Delta=0 \implies sgn(f(x))=
        \left\{\begin{align}
            sgn(a) & \qquad\forall x\in\mathbb{R}\setminus\left\{\dfrac{-b}{2a}\right\} \\
          0 & \qquad x=\dfrac{-b}{2a}\\
        \end{align}\right.$
- Si Δ>0 alors $f(x)$ est du signe de $a$ à l'extérieur de l'intervalle borné par ses racines et de signe contraire dans cet intervalle.<br>
      $\Delta=0 \implies sgn(f(x))=
        \left\{\begin{align}
            sgn(a) & \qquad\forall x\notin[x_0,x_1] \\
            0 & \qquad x\in\{x_0,x_1\} \\
            -sgn(a) & \qquad\forall x\in]x_0,x_1[ \\
        \end{align}\right.$

??? note "La fonction signe"

    La fonction signe est définie par : <br>
    $\begin{align}
        sgn : \mathbb{R} & \rightarrow \{-1,0,1\} & \\
        x & \mapsto \left\{\begin{matrix}
            -1 & \text{si } x<0 \\
            0 & \text{si } x=0 \\
            1 & \text{si } x>0
        \end{matrix}\right.
    \end{align}$

    Elle se représente graphiquement par :<br>

    ![fonction signe](images/fonction_sgn.png)

    C'est une fonction qui présente **une solution de continuité en 0**, cela signifie qu'elle est discontinue en 0, c'est-à-dire que «pour tracer la fonction il faut lever le crayon de la feuille pour passer 0».<br>
    L'expression est une solution de **continuité** pour parler d'un endroit où une fonction est **discontinue** !

## Somme et produit des racines d'une équation du second degré

Soit $f(x)=ax^2+bx+c$ un trinôme du second degré et $\Delta$ son discriminant.

Si $\Delta>0$ alors l'équation $f(x)=0$ possède deux solutions réelles $x_0$ et $x_1$.

De plus on a $x_0+x_1=\dfrac{-b}{a}$ et $x_0x_1=\dfrac{c}{a}$

??? preuve "Preuve"

    Les deux racines sont du trinôme sont :

    $\begin{align}
        x_0 &= \dfrac{-b-\sqrt{\Delta}}{2a} \\
        x_1 &= \dfrac{-b+\sqrt{\Delta}}{2a}
    \end{align}$

    - Calculons leur somme

      $\begin{align}
        x_0+x_1 &= \dfrac{-b-\sqrt{\Delta}}{2a} + \dfrac{-b+\sqrt{\Delta}}{2a} \\
                &= \dfrac{-b-\sqrt{\Delta}+ (-b)+\sqrt{\Delta}}{2a} \\
                &= \dfrac{-2b}{2a} \\
                &= -\dfrac{b}{a}
      \end{align}$

    - Calculons leur produit
      
      $\begin{align}
        x_0+x_1 &= \dfrac{-b-\sqrt{\Delta}}{2a} \times \dfrac{-b+\sqrt{\Delta}}{2a} & \\
                &= \dfrac{ (-b-\sqrt{\Delta})  (-b+\sqrt{\Delta})}{4a^2} & \text{Identité remarquable !}\\
                &= \dfrac{(-b)^2-\sqrt{\Delta}^2}{4a^2} & \\
                &= \dfrac{b^2-\Delta}{4a^2} & \text{comme }\Delta=b^2-4ac\\
                &= \dfrac{b^2-(b^2-4ac)}{4a^2} & \\
                &= \dfrac{4ac}{4a^2} & \\
                &= \dfrac{c}{a}
      \end{align}$

    Nous avons donc bien : $x_0+x_1=\dfrac{-b}{a}$ et $x_0x_1=\dfrac{c}{a}$
    ■
!!!methode "Recherche de deux nombres dont on connaît la somme et le produit"

    Rechercher deux nombres, $x_0$ et $x_1$, dont on ne connaît que la somme $S$ et le produit $P$ revient à résoudre le système d'équations :


    $\sigma:\left\{\begin{align}
            x_0+x_1 &= S \\
            x_0\times x_1 &= P
    \end{align}\right.$

    Sa résolution passe par la résolution de l'équation $E:x^2-Sx+P=0$.

    - Si $S_E=\emptyset$ alors $S_\sigma=\emptyset$
    - Si $S_E=\{x_0\}$ alors $S_\sigma=\{(x_0,x_0)\}$
    - Si $S_E=\{x_0,x_1\}$ alors $S_\sigma=\{(x_0,x_1),(x_1,x_0)\}$

    