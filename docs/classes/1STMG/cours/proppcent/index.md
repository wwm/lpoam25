---
title: Évolutions et pourcentages
---

## Proportion

!!! definition "Calcul de la proportion et du pourcentage"

    Soit un ensemble $E$ comprenant $N$ éléments et soit $A$ une partie de $E$ comprenant $n$ éléments.

    La proportion de $A$ dans $E$ est le rapport du nombre d'éléments de A par rapport à $E$.

    $$\text{proportion}=\dfrac{n}{N}$$

    Le pourcentage repréntée par la partie A est sa proportion par rapport à $100$ :

    $$p=\dfrac{\text{proportion}}{100}=\dfrac{n}{N}\times 100$$

!!! example "Exemple"

    Une classe de 35 élèves comprend 20 filles.

    La proportion de filles dans la classe est de $\dfrac{\text{nombre de filles}}{\text{nombre d'élèves}}=\dfrac{20}{35}=0,571\dots$

    Le pourcentage de filles dans la classe est de $\text{proportion}\times 100=0,571 \times 100=57,1\%$

## Taux d'évolution

!!! definition "Définition"

    Lorsqu'une variable $X$ passe de la valeur $X_{\text{initial}}$ à la valeur $X_{\text{final}}$, le **taux d'évolution** $t_e$ est :

    $$t_e=\dfrac{X_{\text{final}}-X_{\text{initial}}}{X_{\text{initial}}}$$

    Le **pourcentage d'évolution** est la taux d'évolution multiplié par $100$ :

    $$p_e = t_e \times 100 = \dfrac{X_{\text{final}}-X_{\text{initial}}}{X_{\text{initial}}} \times 100$$

!!! warning "Attention"

    - Un taux d'évolution est une grandeur algébrique, cela signifie qu'il peut être négatif. Il peut même être supérieur à $1$.

        **Mais il ne peut être en aucun cas inférieur à $-1$**.

        Si $t_e>0$ il s'agit d'une **augmentation**<br>
        Si $t_e<0$ il s'agit d'une **diminution**


    - Un pourcentage d'évolution est une grandeur algébrique, cela signifie qu'il peut être négatif. Il peut même être supérieur à $100\%$.

        **Mais il ne peut être en aucun cas inférieur à $-100\%$**.

        Si $p_e>0$ il s'agit d'une **augmentation**<br>
        Si $p_e<0$ il s'agit d'une **diminution**

!!! example "Exemple"

    - Le prix d'un article est passé de 4,85€ à7,20€.<br>
      Le **taux d'évolution** est : $t_e=\dfrac{7,20-4,85}{4,85}=0,4845$ ⇒ il s'agit d'une hausse de $48,45\%$

    - La production d'une usine est passée de 435 tonnes à 278 tonnes.<br>
      Le **pourcentage d'évolution** est : $p_e=\dfrac{278-435}{435}\times 100=-36,09\%$ (baisse de $36,09\%$)

!!! parcoeur "À retenir"

    C'est toujours la valeur initiale qui est au dénominateur et, attention, ce n'est *pas toujours* la plus petite

## Coefficient d'évolution

!!! definition "Définition"

    Lorsqu'une variable $X$ subit un pourcentage d'évolution égal à $t_e \%$ à partir d'une valeur $X_{\text{initial}}$, la nouvelle valeur $X_{\text{final}}$ est :

    $$X_{\text{final}}=X_{\text{initial}}+\dfrac{t_e}{100}{X_{\text{initial}}}= X_{\text{initial}} \left( 1+\dfrac{t_e}{100} \right)$$

    Le nombre $k=1+\dfrac{t_e}{100}=\dfrac{X_{\text{final}}}{X_{\text{initial}}}$ est appelé **coefficient d'évolution** ou **coefficient multiplicateur** de la variable $X$.

    !!! warning "Attention"

        Si $k>1$ alors il s'agit d'une **augmentation**

        Si $k<1$ alors il s'agit d'une **diminution**        

!!! methode "Application pratique"

    Augmenter une grandeur de $t\%$ revient à multiplier par $k=1+\dfrac{t}{100}$

    Diminuer une grandeur de $t\%$ revient à multiplier par $k=1-\dfrac{t}{100}$

    Réciproquement, si une grandeur subit un coefficient d'évolution égal à $k$, alors le pourcentage d'évolution est : $t=100(k-1)$.

    - Si le coefficient d'évolution est égal à $1,23$ alors il s'agit d'une augmentation de $1,23-1=23\%$.
    - Si le coefficient d'évolution est égal à $0,23$ alors il s'agit d'une diminution de $1-0,23=0,77\%$.

!!! exemple "Examples"

    - Pour une hausse de $150\%$, le coefficient d'évolution est $k=1+\dfrac{150}{100}=2,5$
    - Pour une baisse de $32\%$, le coefficient d'évolution est $k=1-\dfrac{32}{100}=0,68$
    - Si un prix passe de 28,50€ à 31€ le coefficient multiplicateur est : $k=\dfrac{31}{28,5}=1,0877$<br>
      soit une hausse de $8,77\%$

## Évolutions successives

!!! methode "Manipuler les évolutions successives"

    Lorsqu'une grandeur subit plusieurs variations successives (hausses ou baisses), pour obtenir le coefficient multiplicateur global, on **multiplie** ensemble les coefficients multiplicateurs.

!!! exemple "Examples"

    Le prix d'un article subit successivement une hausse de $30\%$ suivie d'une baisse de $20\%$ et d'une nouvelle baisse de $10\%$.<br>
    Quel est le pourcentage global d'évolution ?

    1. Le coefficient multiplicateur lors de la hausse est $k_1=1+\dfrac{30}{100}=1,3$
    1. Le coefficient multiplicateur lors de la première baisse est $k_2=1-\dfrac{20}{100}=0,8$
    1. Le coefficient multiplicateur lors de la seconde baisse est $k_3=1-\dfrac{10}{100}=0,9$
    1. Le coefficient multiplicateur global est $k=k_1 k_2 k_3=1,3\times0,8\times0,9=0,936$
    1. Le pourcentage global d'évolution est $t=100(k-1)=100(0,936-1)=-6,4\%$

    L'évolution globale est donc une baisse de $6,4\%$.

!!! danger "Attention"

    Lors d'évolutions successives, **on n'ajoute {++jamais++} les pourcentages d'évolutions !**

    L'ordre des évolutions successives n'a aucune importance.

## Taux d'évolution réciproque

!!! definition "Définition"
    Si une variable $X$ subit un taux d'évolution $t$ pour passer de la valeur $X_\text{initial}$ à une valeur $X_\text{final}$, alors
    le **taux d'évolution réciproque** $t_r$ est le taux qu'il faut appliquer à $X_\text{final}$ pour revenir à la valeur $X_\text{initial}$.

!!! methode "Comment le calculer"

    Si la variable $X$ subit une évolution de taux $t$, le coefficient multiplicateur est $k=1+\dfrac{t}{100}$.<br>
    Pour revenir à la valeur d'origine il va falloir multiplier la valeur finale par $k_r=\dfrac{1}{k}$

    $$\begin{align}
        k_r &= 1+\dfrac{t_r}{100} &\ \text{par définition}\\
        \dfrac{1}{k} &= 1+\dfrac{t_r}{100} &\  \text{on utilise le fait que }k_r=\dfrac{1}{k}\\
        \dfrac{1}{1+\dfrac{t}{100}} &= 1+\dfrac{t_r}{100} &\  \text{par définition }k=1+\dfrac{t}{100}\\
        100\left( \dfrac{1}{1+\dfrac{t}{100}} - 1\right) &= t_r &\ \text{on isole } t_r \\
    \end{align}$$

!!! parcoeur "À retenir"

    Le taux d'évolution réciproque $t_r$ d'une taux $t$ est :

    $$t_r=100\left( \dfrac{1}{1+\dfrac{t}{100}}-1\right)$$

!!! example "Exemple"

    Si on a une hausse de $t=20\%$, le pourcentage de baisse pour revenir à la même valeur est : $t_r=100\left( \dfrac{1}{1+\dfrac{20}{100}}-1\right) \approx -16,67\%$

    !!! warning "!!!"
        Une hausse de $20\%$ n'est pas compensée par une baisse de $20\%$ !<br>
        Elle est compensée par une baisse d'environ de $16,67\%$