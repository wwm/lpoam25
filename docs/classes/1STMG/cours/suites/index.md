---
title: Les suites numériques
---

Le mot **suite** désigne simplement des éléments qui sont rangés en ordre croissant. Ces éléments peuvent être aussi bien des figures, que des nombres par exemple. Il s'agit ensuite de trouver une **relation** qui lie tous ces éléments ensembles.

## Activité : figures en allumettes

### Tous en ligne !
Dans cette activité nous allons étudier quelques propriétés de figures qui ressemblent à des carrés qui se suivent et qui sont construites avec des allumettes.

On peut par exemple construire :

![les carrés en allumettes](images/carres_01.png)

En utilisant l'appliquette geogebra sur votre ordinateur :

??? geogebra "suite horizontale de carrés"

    <iframe scrolling="no" title="suite_01" src="https://www.geogebra.org/material/iframe/id/x6sdxjx9/width/1920/height/895/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1920px" height="700px" style="border:0px;"> </iframe>

Remplissez les tableaux suivants dans votre cours papier :

| numéro de la figure | 1   | 2   | 3   | 4   | 5   |
| ------------------- | --- | --- | --- | --- | --- |
| nombre d'allumettes |     |     |     |     |     |
| périmètre           |     |     |     |     |     |
| aire                |     |     |     |     |     |


Conjecturez (devinez) comment les valeurs évoluent en fonction de la figure 

!!! note "Conjecture"
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________
    ________________________________________________________________________

Remplissez ce tableau et vérifiez ensuite avec l'appliquette que votre conjecture est correcte :

| numéro de la figure | 10  | 11  | 12  | 13  |
| ------------------- | --- | --- | --- | --- |
| nombre d'allumettes |     |     |     |     |
| périmètre           |     |     |     |     |
| aire                |     |     |     |     |


| numéro de la figure | 17  | 18  | 19  | 20  |
| ------------------- | --- | --- | --- | --- |
| nombre d'allumettes |     |     |     |     |
| périmètre           |     |     |     |     |
| aire                |     |     |     |     |

### Prenons un peu de hauteur

Intéressons-nous maintenant aux figures suivantes :

![carrés 02](images/carres_02.png)

À votre avis, de combien de carrés la figure 8 sera-t-elle composée ?

Vous pouvez vous aider de l'appliquette geogebra :

??? geogebra "carrés et perpendiculaire"

    <iframe scrolling="no" title="suite_02" src="https://www.geogebra.org/material/iframe/id/tbexwwjb/width/1920/height/895/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1920px" height="895px" style="border:0px;"> </iframe>

- étape 1 : faire un tableau sur les premières valeurs
  
| numéro de la figure | 1   | 2   | 3   | 4   |
| ------------------- | --- | --- | --- | --- |
| nombre d'allumettes |     |     |     |     |

- étape 2 : comment le nombre d'allumettes change-t-il lorsqu'on passe d'une figure à une autre ?

    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
- étape 3 : essayer de trouver une formule qui lie le numéro de la figure, qu'on va noter $n$, avec le nombres d'allumettes, que l'on va noter $a_n$ ($a$ indice $n$)
  
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________

- étape 4 : calculer à l'aide de la formule conjecturée la valeur du nombre d'allumettes de la figure 8, c'est-à-dire $a_8$
  
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________
    
    ________________________________________________________________________

### Avec un tableur

Téléchargez le fichier [suite_02.csv](fichiers/suite_02.csv). Ouvrez-le avec un tableur (openoffice calc/excel).

Voici ce que vous devriez voir :

![tableur](images/tableur_02.png)

À faire :

1. Remplissez la colonne B
2. Complétez la colonne C avec votre formule conjecturée
3. Bonus : construisez un graphique comparant les résultat de votre formule avec le décompte réel