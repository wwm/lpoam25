---
title: Suites numériques
---

## Calculs de termes

### Déterminer les termes d'une suite définie de façon explicite

<iframe src="https://coopmaths.fr/alea/?uuid=f0c2d&id=1AL10-3&n=4&d=10&s=1&alea=QQ5j&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Calculer les termes d'une suite arithmétique

<iframe src="https://coopmaths.fr/alea/?uuid=3ae4a&id=1AL11-4&n=4&d=10&s=1-2&alea=LPrh&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>


### Calculer les termes d'une suite géométrique

<iframe src="https://coopmaths.fr/alea/?uuid=3ae4a&id=1AL11-4&n=4&d=10&s=7&i=1&cd=0&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>