---
title: Évolutions et pourcentages
---

## Connaitre les différentes écritures d'une proportion

### Décimal vers fraction et pourcentage

<iframe src="https://coopmaths.fr/alea/?uuid=ae913&id=2S10-1&n=4&d=10&s=1&alea=NkOe&i=1&cd=1&cols=2&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Pourcentage vers fraction et décimal

<iframe src="https://coopmaths.fr/alea/?uuid=ae913&id=2S10-1&n=4&d=10&s=2&alea=NkOe&i=1&cd=1&cols=2&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Fraction vers pourcentage et décimal

<iframe src="https://coopmaths.fr/alea/?uuid=ae913&id=2S10-1&n=4&d=10&s=3&alea=NkOe&i=1&cd=1&cols=2&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Tout en même temps

<iframe src="https://coopmaths.fr/alea/?uuid=ae913&id=2S10-1&n=6&d=10&s=4&alea=NkOe&i=1&cd=1&cols=2&v=eleve&es=0211000&title=" width="100%" height="600px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Calculer une proportion ou appliquer un pourcentage

### Déterminer l'effectif d'une sous-population

<iframe src="https://coopmaths.fr/alea/?uuid=612a5&id=2S10-2&n=4&d=10&s=1&alea=ankN&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Calculer une proportion en pourcentage

<iframe src="https://coopmaths.fr/alea/?uuid=612a5&id=2S10-2&n=4&d=10&s=2&alea=ankN&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Calculer l'effectif de la population totale

<iframe src="https://coopmaths.fr/alea/?uuid=612a5&id=2S10-2&n=4&d=10&s=3&alea=ankN&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Tout en même temps

<iframe src="https://coopmaths.fr/alea/?uuid=612a5&id=2S10-2&n=4&d=10&s=4&alea=ankN&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Taux d'évolution

### Déterminer le résultat après une évolution

<iframe src="https://coopmaths.fr/alea/?uuid=12444&id=2S11-2&n=4&d=10&s=1&alea=IUdS&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Passer du taux d'évolution au coefficient d'évolution

<iframe src="https://coopmaths.fr/alea/?uuid=4b11f&id=can2C10&n=6&d=10&alea=fODu&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

### Calculer un taux d'évolution

<iframe src="https://coopmaths.fr/alea/?uuid=12444&id=2S11-2&n=4&d=10&s=2&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>

## Taux d'évolution réciproque

### Déterminer un taux d'évolution réciproque

<iframe src="https://coopmaths.fr/alea/?uuid=509db&id=2S12-3&n=4&d=10&s=4&alea=TAQI&i=1&cd=1&v=eleve&es=0211000&title=" width="100%" height="500px" frameborder="0" allowfullscreen class="shadow-splash-min"></iframe>