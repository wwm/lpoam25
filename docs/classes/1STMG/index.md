---
title: Première S.T.M.G.
---

## Pourquoi les maths ?

L’acquisition d’une culture mathématique est nécessaire pour évoluer dans un environnement numérique, dans lequel les données et les graphiques occupent une large place. L’objectif est de développer des aptitudes intellectuelles indispensables à la réussite d’études supérieure. 

## Les objectifs de cette année

Le programme de mathématiques en classe de 1<sup>ère</sup> techno est organisé en trois parties transversales :

- vocabulaire ensembliste et logique
- algorithmique et programmation
- automatismes

et en deux parties thématiques :

- analyse pour étudier ou modéliser des évolutions
- statistiques et probabilités pour traiter et interpréter des données, pour modéliser des phénomènes aléatoires

## Progression

::timeline::
[
    {
        "title": "Présentation",
        "sub_title": "1",
        "content": "Réactivation automatismes seconde",
        "icon": ":fontawesome-solid-person-chalkboard:",
    },
    {
        "title": "Proportions, pourcentages et évolutions",
        "sub_title": "2, 3, 4",
        "content": "...",
        "icon": ":material-format-superscript:"
    },
    {
        "title": "Suites arithmétiques et géométriques",
        "sub_title": "5, 6, 7",
        "content": "...",
        "icon": ":material-function:"
    },
    {
        "title": "Généralités sur les fonctions",
        "sub_title": "10, 11, 12, 13",
        "content": "...",
        "icon": ":material-cards-playing-outline:"
    },
    {
        "title": "Tableaux croisés et probabilités conditionnelles",
        "sub_title": "14, 15, 16",
        "content": "...",
        "icon": ":material-function:"
    },
    {
        "title": "Fonction polynômes de degré 2",
        "sub_title": "19, 20, 21",
        "content": "...",
        "icon": ":material-circle-medium:"
    },
    {
        "title": "Dérivation",
        "sub_title": "22, 23, 24",
        "content": "...",
        "icon": ":material-exponent:"
    },
    {
        "title": "Variables aléatoires",
        "sub_title": "25,26,27,28",
        "content": "...",
        "icon": ":material-alpha-n:"
    },
    {
        "title": "Fonction polynômes de degré 3",
        "sub_title": "29, 30, 31",
        "content": "...",
        "icon": ":material-tray-remove:"
    },
    {
        "title": "Suites de références",
        "sub_title": "34, 35, 36",
        "content": "...",
        "icon": ":material-alpha-n:"
    },
]
::/timeline::