---
title: Mathématiciennes et mathématiciens célèbre
collapse: false
---

## Seconde

[1001 exercices corrigés de mathématiques pour réussir son_année - Deuxième_édition - Konrad Renard](/docs/books/1001_exercices_corrigés_de_mathématiques_pour_réussir_son_année__Deuxième_édition___Konrad_Renard_.pdf)

[Méthod'X Mathématiques Seconde - Thomas Petit](/docs/books/Mathématiques_-_Seconde_-_nouveaux_programmes__Thomas_Petit_.pdf)